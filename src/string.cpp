/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rexy/string.hpp"
#include "rexy/allocator.hpp"

namespace rexy{

	template class basic_string<char,allocator<char>>;
	template class basic_string<wchar_t,allocator<wchar_t>>;
	template class basic_string<char16_t,allocator<char16_t>>;
	template class basic_string<char32_t,allocator<char32_t>>;
#ifdef __cpp_char8_t
	template class basic_string<char8_t,allocator<char8_t>>;
#endif

}
