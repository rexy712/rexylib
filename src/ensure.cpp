//Never actually used in the project. This just ensures that all syntax is correct during builds.

#include "rexy/rexy.hpp"
#include "rexy/algorithm.hpp"
#include "rexy/allocator.hpp"
#include "rexy/buffer.hpp"
#include "rexy/buffer.tpp"
#include "rexy/expression.hpp"
#include "rexy/hash.hpp"
#include "rexy/mpmc_queue.hpp"
#include "rexy/steal.hpp"
#include "rexy/string_base.hpp"
#include "rexy/string_base.tpp"
#include "rexy/string.hpp"
#include "rexy/traits.hpp"
#include "rexy/utility.hpp"
#include "rexy/meta.hpp"
#include "rexy/enum_traits.hpp"
#include "rexy/deferred.hpp"
#include "rexy/debug_print.hpp"
#include "rexy/storage_for.hpp"
#include "rexy/visitor.hpp"
#include "rexy/string_view.hpp"
#include "rexy/string_view.tpp"
#include "rexy/list.hpp"
#include "rexy/list.tpp"

#ifndef LIBREXY_HEADER_ONLY
	#include "rexy/filerd.hpp"
	#include "rexy/threadpool.hpp"
	#include "rexy/demangle.hpp"
#endif

#include "rexy/detail/string_appender.hpp"

#include "rexy/cx/array.hpp"
#include "rexy/cx/hashmap.hpp"
#include "rexy/cx/string.hpp"
#include "rexy/cx/vector.hpp"

#include "rexy/cx/detail/bool_specialize_base.hpp"

#include "rexy/compat/to_address.hpp"
#include "rexy/compat/source_location.hpp"
