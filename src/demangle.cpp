/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rexy/demangle.hpp"

#if defined(__GNUC__) && !defined(_MSC_VER)

	#include <cstdlib> //free
	#include <string>
	#include <memory> //unique_ptr
	#include <cxxabi.h> //__cxa_demangle

	namespace util{
		std::string demangle(const char* name){
			int status = 0;
			std::unique_ptr<char,void(*)(void*)> res{
				abi::__cxa_demangle(name, nullptr, nullptr, &status),
				std::free
			};
			return (status == 0) ? res.get() : name;
		}
	}

#else

	#include <string>
	namespace util{
		std::string demangle(const char* name){
			return name;
		}
	}

#endif
