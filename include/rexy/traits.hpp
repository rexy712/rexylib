/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_TRAITS_HPP
#define REXY_TRAITS_HPP

#include <type_traits> //is_same, decay, integral_constant, declval
#include <iterator> //iterator_traits

#include "compat/traits.hpp"
#include "rexy.hpp"

namespace rexy{

	template<class T, class U>
	struct is_type{
		static std::true_type check(U*);
		static std::false_type check(...);

		static constexpr bool value = decltype(check(std::declval<std::decay_t<T>*>()))::value;
	};

	template<class T, template<class...> class U>
	struct is_template_type_helper{
		static constexpr bool value = false;
	};
	template<template<class...> class U, class... Args>
	struct is_template_type_helper<U<Args...>,U>{
		static constexpr bool value = true;
	};
	template<class T, template<class...> class U>
	struct is_template_type : public is_template_type_helper<std::decay_t<T>,U>{};

	template<class T, template<class...> class Tmpl>
	struct is_template_derived_type{
		template<class... Args>
		static std::true_type test(Tmpl<Args...>*);
		static std::false_type test(void*);

		static constexpr bool value = std::is_same<std::true_type,decltype(test(static_cast<std::decay_t<T>*>(nullptr)))>::value;
	};


	template<class T, class = void>
	struct is_dereferencable : public std::false_type{};
	template<class T>
	struct is_dereferencable<T,std::void_t<decltype(*(std::declval<T>()))>> : public std::true_type{};
	template<class T>
	static constexpr bool is_dereferencable_v = is_dereferencable<T>::value;

	template<class T, class = void>
	struct is_pointer_dereferencable : public std::false_type{};
	template<class T>
	struct is_pointer_dereferencable<T,std::void_t<decltype(std::declval<T>().operator->())>> : public std::true_type{};
	template<class T>
	struct is_pointer_dereferencable<T*,std::void_t<T*>> : public std::true_type{};
	template<class T>
	static constexpr bool is_pointer_dereferencable_v = is_pointer_dereferencable<T>::value;

	template<class T, class = void>
	struct is_prefix_incrementable : public std::false_type{};
	template<class T>
	struct is_prefix_incrementable<T,std::void_t<decltype(++(std::declval<std::add_lvalue_reference_t<remove_cvref_t<T>>>()))>> : public std::true_type{};
	template<class T>
	static constexpr bool is_prefix_incrementable_v = is_prefix_incrementable<T>::value;

	template<class T, class = void>
	struct is_postfix_incrementable : public std::false_type{};
	template<class T>
	struct is_postfix_incrementable<T,std::void_t<decltype((std::declval<std::add_lvalue_reference_t<remove_cvref_t<T>>>())++)>> : public std::true_type{};
	template<class T>
	static constexpr bool is_postfix_incrementable_v = is_postfix_incrementable<T>::value;

	template<class T, class = void>
	struct is_equality_comparable : public std::false_type{};
	template<class T>
	struct is_equality_comparable<T,std::void_t<decltype(std::declval<T>() == std::declval<T>())>> : public std::true_type{};
	template<class T>
	static constexpr bool is_equality_comparable_v = is_equality_comparable<T>::value;

	template<class T, class = void>
	struct is_inequality_comparable : public std::false_type{};
	template<class T>
	struct is_inequality_comparable<T,std::void_t<decltype(std::declval<T>() != std::declval<T>())>> : public std::true_type{};
	template<class T>
	static constexpr bool is_inequality_comparable_v = is_inequality_comparable<T>::value;

	template<class T>
	struct is_legacy_iterator{
		static constexpr bool value = is_prefix_incrementable_v<T> && is_dereferencable_v<T>;
	};
	template<class T>
	static constexpr bool is_legacy_iterator_v = is_legacy_iterator<T>::value;

	template<class T, bool = is_legacy_iterator_v<T> &&
	                         is_inequality_comparable_v<T> &&
	                         is_pointer_dereferencable_v<T> &&
	                         is_postfix_incrementable_v<T>>
	struct is_legacy_input_iterator : public std::false_type{};

	template<class T>
	struct is_legacy_input_iterator<T,true>{
		static constexpr bool value = std::is_convertible_v<decltype(std::declval<T>() == std::declval<T>()),bool>  &&
		                              std::is_same_v<decltype(*(std::declval<T>())),typename std::iterator_traits<T>::reference> &&
		                              std::is_same_v<decltype(++std::declval<std::add_lvalue_reference_t<remove_cvref_t<T>>>()),std::add_lvalue_reference_t<T>> &&
		                              std::is_convertible_v<decltype(*std::declval<std::add_lvalue_reference_t<remove_cvref_t<T>>>()++), typename std::iterator_traits<T>::value_type>;
	};
	template<class T>
	static constexpr bool is_legacy_input_iterator_v = is_legacy_input_iterator<T>::value;

	template<class T, class Value, bool = is_legacy_iterator_v<T> &&
	                                      is_postfix_incrementable_v<T>>
	struct is_legacy_output_iterator : public std::false_type{};

	template<class T, class Value>
	struct is_legacy_output_iterator<T,Value,true>{
		static constexpr bool value =
			std::is_same_v<decltype(++std::declval<std::add_lvalue_reference_t<remove_cvref_t<T>>>()),std::add_lvalue_reference_t<T>> &&
			std::is_convertible_v<decltype(std::declval<std::add_lvalue_reference_t<remove_cvref_t<T>>>()++), std::add_lvalue_reference_t<std::add_const_t<T>>>;
	};
	template<class T, class Value>
	static constexpr bool is_legacy_output_iterator_v = is_legacy_output_iterator<T,Value>::value;

}

#endif
