/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_FORMAT_HPP
#define REXY_FORMAT_HPP

#include "string.hpp"
#include "string_view.hpp"

#include "compat/standard.hpp"

REXY_REQUIRES_CPP20;

#ifndef REXY_STANDARD_CPP20
	//Maybe I'll make it work someday, but not for now
	#error "Cannot use formatting library without C++20 support"
#endif

#include "detail/format/standard_types.hpp"
#include "detail/format/internal_types.hpp"
#include "detail/format/arg_store.hpp"
#include "detail/format/format_string.hpp"
#include "detail/format/format_args.hpp"
#include "detail/format/format_error.hpp"

#include <cstdio> //FILE

#include <cstddef> //size_t
#include <locale> //locale

namespace rexy{

	//Alias declarations of standard defined types
	using format_context = fmt::detail::fmt_context_t<char>;
	using wformat_context = fmt::detail::fmt_context_t<wchar_t>;
	using parse_context = fmt::detail::parse_context_t<char>;
	using wparse_context = fmt::detail::parse_context_t<wchar_t>;
	using format_arg = fmt::basic_format_arg<format_context>;
	using wformat_arg = fmt::basic_format_arg<wformat_context>;
	using format_args = fmt::basic_format_args<format_context>;
	using wformat_args = fmt::basic_format_args<wformat_context>;

	template<class Context = format_context, class... Args>
	fmt::detail::basic_format_arg_store<Context,Args...> make_format_args(Args&&... args);
	template<class Context = wformat_context, class... Args>
	fmt::detail::basic_format_arg_store<Context,Args...> make_wformat_args(Args&&... args);

	template<class OutIt>
	OutIt vformat_to(OutIt out, string_view fmt, format_args args);
	template<class OutIt>
	OutIt vformat_to(OutIt out, const std::locale& loc, string_view fmt, wformat_args args);

	string vformat(string_view fmt, format_args args);
	string vformat(const std::locale& loc, string_view fmt, format_args args);

	template<class OutIt, class... Args>
	OutIt format_to(OutIt out, fmt::detail::format_string<Args...> fmt, Args&&... args);
	template<class OutIt, class... Args>
	OutIt format_to(OutIt out, const std::locale& loc, fmt::detail::format_string<Args...> fmt, Args&&... args);

	template<class OutIt, class... Args>
	format_to_n_result<OutIt> format_to_n(OutIt out, std::size_t max, fmt::detail::format_string<Args...> fmt, Args&&... args);
	template<class OutIt, class... Args>
	format_to_n_result<OutIt> format_to_n(OutIt out, const std::locale& loc, std::size_t max, fmt::detail::format_string<Args...> fmt, Args&&... args);

	template<class... Args>
	string format(fmt::detail::format_string<Args...> fmt, Args&&... args);
	template<class... Args>
	string format(const std::locale& loc, fmt::detail::format_string<Args...> fmt, Args&&... args);

	template<class... Args>
	std::size_t formatted_size(fmt::detail::format_string<Args...> fmt, Args&&... args);
	template<class... Args>
	std::size_t formatted_size(const std::locale& loc, fmt::detail::format_string<Args...> fmt, Args&&... args);




	template<class OutIt>
	OutIt vformat_to(OutIt out, wstring_view fmt, wformat_args args);
	template<class OutIt>
	OutIt vformat_to(OutIt out, const std::locale& loc, wstring_view fmt, wformat_args args);

	wstring vformat(wstring_view fmt, format_args args);
	wstring vformat(const std::locale& loc, wstring_view fmt, wformat_args args);

	template<class OutIt, class... Args>
	OutIt format_to(OutIt out, fmt::detail::wformat_string<Args...> fmt, Args&&... args);
	template<class OutIt, class... Args>
	OutIt format_to(OutIt out, const std::locale& loc, fmt::detail::wformat_string<Args...> fmt, Args&&... args);

	template<class OutIt, class... Args>
	format_to_n_result<OutIt> format_to_n(OutIt out, std::size_t max, fmt::detail::wformat_string<Args...> fmt, Args&&... args);
	template<class OutIt, class... Args>
	format_to_n_result<OutIt> format_to_n(OutIt out, const std::locale& loc, std::size_t max, fmt::detail::wformat_string<Args...> fmt, Args&&... args);

	template<class... Args>
	wstring format(fmt::detail::wformat_string<Args...> fmt, Args&&... args);
	template<class... Args>
	wstring format(const std::locale& loc, fmt::detail::wformat_string<Args...> fmt, Args&&... args);

	template<class... Args>
	std::size_t formatted_size(fmt::detail::wformat_string<Args...> fmt, Args&&... args);
	template<class... Args>
	std::size_t formatted_size(const std::locale& loc, fmt::detail::wformat_string<Args...> fmt, Args&&... args);




	template<class... Args>
	std::size_t print(fmt::detail::format_string<Args...> fmt, Args&&... args);
	template<class... Args>
	std::size_t print(FILE* stream, fmt::detail::format_string<Args...> fmt, Args&&... args);

	template<class... Args>
	std::size_t println(fmt::detail::format_string<Args...> fmt, Args&&... args);
	template<class... Args>
	std::size_t println(FILE* stream, fmt::detail::format_string<Args...> fmt, Args&&... args);

	std::size_t vprint_unicode(string_view fmt, format_args args);
	std::size_t vprint_unicode(FILE* stream, string_view fmt, format_args args);



	template<class T, class Char>
	constexpr auto arg(const Char* name, T&& t);
	template<class T, class Char>
	constexpr auto arg(rexy::basic_string_view<Char> name, T&& t);
	template<rexy::cx::string Name, class T>
	constexpr auto arg(T&& t);

	inline namespace fmt_literals{
		template<rexy::cx::string Name>
		constexpr auto operator""_a(void);
	}

}


#include "format.tpp"

#endif
