/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_HASH_HPP
#define REXY_HASH_HPP

#include <climits> //CHAR_BIT
#include <cstddef> //size_t
#include "rexy.hpp"

#include "allocator.hpp"

namespace rexy{

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	class basic_string;
	template<class Char>
	class basic_string_view;
	namespace cx{
		template<std::size_t N, class Char>
		class string;
	}

	template<class T>
	struct hash{
		constexpr std::size_t operator()(const T& t, std::size_t salt = 0)const noexcept{
			constexpr std::size_t bytes = sizeof(std::size_t);
			std::size_t val = static_cast<std::size_t>(t);
			std::size_t hash = 5381 + salt; //magic hash number
			for(std::size_t i = 0;i < bytes;++i){
				unsigned char c = static_cast<unsigned char>(val >> (i * CHAR_BIT));
				hash = ((hash << 5) + hash) ^ c;
			}
			return hash;
		}
	};

	namespace detail{
		//jenkins one at a time hash
		template<class Str>
		struct string_hash{
			constexpr std::size_t operator()(const Str& s, std::size_t salt = 0)const noexcept{
				std::size_t hash = salt;
				for(std::size_t i = 0;i < s.length();++i){
					hash += s[i];
					hash += (hash << 10);
					hash += (hash >> 6);
				}
				hash += (hash << 3);
				hash ^= (hash << 11);
				hash += (hash << 15);
				return hash;
			}
		};
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	struct hash<rexy::basic_string<Char,Alloc>> : public detail::string_hash<rexy::basic_string<Char,Alloc>>{};
	template<class Char>
	struct hash<rexy::basic_string_view<Char>> : public detail::string_hash<rexy::basic_string_view<Char>>{};
	template<std::size_t N, class Char>
	struct hash<rexy::cx::string<N,Char>> : public detail::string_hash<rexy::cx::string<N,Char>>{};

}

#endif
