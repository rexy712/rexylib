/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STRING_APPENDER_HPP
#define REXY_STRING_APPENDER_HPP

#include "../expression.hpp"
#include <utility> //forward
#include <type_traits> //enable_if, declval

namespace rexy::detail{

	template<class Targ>
	struct string_appender
	{
	private:
		Targ& m_targ;
	public:
		constexpr string_appender(Targ& t)noexcept:
			m_targ(t){}
		template<class L, class R>
		constexpr void operator()(const binary_expression<L,R>& expr)
			noexcept(noexcept((*this)(expr.left())) && noexcept((*this)(expr.right())))
		{
			(*this)(expr.left());
			(*this)(expr.right());
		}
		template<class L>
		constexpr void operator()(const unary_expression<L>& expr)
			noexcept(noexcept((*this)(expr.left())))
		{
			(*this)(expr.left());
		}
		template<class Val, std::enable_if_t<!rexy::is_template_derived_type<Val,binary_expression>::value,int> = 0, class = decltype(std::declval<Val>().length())>
		constexpr void operator()(Val&& v)
		{
			m_targ.append(std::forward<Val>(v).data(), std::forward<Val>(v).length());
		}
	};


}

#endif
