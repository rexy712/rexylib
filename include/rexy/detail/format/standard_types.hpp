/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_STANDARD_TYPES_HPP
#define REXY_DETAIL_FORMAT_STANDARD_TYPES_HPP

namespace rexy::fmt{

	template<class T, class Char>
	class formatter;
	template<class Context>
	class basic_format_arg;
	template<class Context>
	class basic_format_args;
	template<class OutIt, class Char>
	class basic_format_context;
	template<class Char>
	class basic_format_parse_context;
	struct format_error;

}

#endif
