/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_NAMED_ARGS_TPP
#define REXY_DETAIL_FORMAT_NAMED_ARGS_TPP

#include "named_args.hpp"

#include "basic_types.hpp"
#include "../../utility.hpp" //strcmp

namespace rexy::fmt::detail{

	template<class It, std::size_t I, class Arg, class... Args>
	constexpr std::size_t find_static_named_arg_id_impl(It first, It last){
		if constexpr(StaticNamedArg<Arg>){
			const auto len = last - first;
			const auto arg_len = Arg::static_name.length();
			if(len == arg_len){
				if(!rexy::strncmp(first, Arg::static_name.c_str(), arg_len)){
					return I;
				}
			}
		}
		if constexpr(sizeof...(Args) > 0){
			return find_static_named_arg_id_impl<It,I+1,Args...>(first, last);
		}else{
			return invalid_arg_index;
		}
	}
	template<class It, class... Args>
	constexpr std::size_t find_static_named_arg_id(It first, It last){
		if constexpr(sizeof...(Args) == 0){
			return invalid_arg_index;
		}else{
			return find_static_named_arg_id_impl<It, 0, Args...>(first, last);
		}
	}

}

#endif
