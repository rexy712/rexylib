/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_FORMAT_ERROR_HPP
#define REXY_DETAIL_FORMAT_FORMAT_ERROR_HPP

#include "../../string_view.hpp"

#include "../../detail/debug_config.hpp"

#define REXY_FORMAT_REAL_STRINGIFY(str) #str
#define REXY_FORMAT_STRINGIFY(str) REXY_FORMAT_REAL_STRINGIFY(str)
#if LIBREXY_ENABLE_DEBUG_LEVEL > 0
	#define REXY_THROW_FORMAT_ERROR(errstring) throw format_error{__FILE__ ":" REXY_FORMAT_STRINGIFY(__LINE__) ": " errstring}
#else
	#define REXY_THROW_FORMAT_ERROR(errstring) throw format_error{errstring}
#endif

namespace rexy::fmt{

	struct format_error{
		string_view errstr;

		constexpr const char* what(void)const{
			return errstr.c_str();
		}

	};

}

#endif
