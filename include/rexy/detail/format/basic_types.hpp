/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_BASIC_TYPES_HPP
#define REXY_DETAIL_FORMAT_BASIC_TYPES_HPP

#include <cstddef> //size_t

#include "storage.hpp"

namespace rexy::fmt::detail{

	static constexpr std::size_t invalid_arg_index = std::size_t(-1);


	template<class Char>
	struct format_string_view{
		const Char* name;
		std::size_t length;
	};

	struct arg_info{
		storage_type type = storage_type::none_t;
		std::size_t offset = 0;
		bool named = false;
	};

	enum class alignment : int{
		none = 0,
		left = '<',
		right = '>',
		center = '^'
	};
	enum class presentation{
		default_t,
		int_t,
		char_t,
		float_t,
		string_t,
		ptr_t
	};

	struct format_specs{
		int width = 0;
		int precision = 0;
		int type = 0;
		alignment align = alignment::none;
		presentation present = presentation::default_t;
		int align_char = ' ';
		int sign = 0;
		bool locale = false;
		bool alt_form = false;
		bool zero_fill = false;
	};
	enum class dyn_type{
		none = 0,
		index,
		string
	};
	template<class Char>
	struct dynamic_format_specs : public format_specs{
		union dyn_val{
			std::size_t index = invalid_arg_index;
			format_string_view<Char> name;
		}dyn_width, dyn_precision;

		dyn_type width_type = dyn_type::index;
		dyn_type precision_type = dyn_type::index;
	};
	template<class FormatCtx>
	constexpr void normalize_dynamic_format_specs(FormatCtx& ctx, dynamic_format_specs<typename FormatCtx::char_type>& specs);

}

namespace rexy{

	template<class OutIt>
	struct format_to_n_result{
		OutIt out;
		std::size_t size;
	};

}

#endif
