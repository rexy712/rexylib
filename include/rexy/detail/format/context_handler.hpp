/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_CONTEXT_HANDLER_HPP
#define REXY_DETAIL_FORMAT_CONTEXT_HANDLER_HPP

#include <cstddef> //size_t

#include "standard_types.hpp"
#include "internal_types.hpp"
#include "format_context.hpp"
#include "parse_context.hpp"
#include "format_args.hpp"

#include "../../string_view.hpp"

#include <locale> //locale

namespace rexy::fmt::detail{

	//Holds the format and parse contexts and performs parse+format calls for replacement fields
	template<class Char>
	struct format_handler
	{
	public:
		using char_type   = Char;
		using fmt_ctx_t   = fmt_context_t<char_type>;
		using parse_ctx_t = parse_context_t<char_type>;
		using out_it_t    = output_iterator_t<char_type>;
		using fmt_it_t    = typename parse_ctx_t::iterator;
		using handle_t    = typename basic_format_arg<fmt_ctx_t>::handle;
		template<class T>
		using fmter_t     = typename fmt_ctx_t::template formatter_type<T>;

	public:
		fmt_ctx_t   fmt_ctx;
		parse_ctx_t parse_ctx;

	public:
		format_handler(out_it_t output, basic_string_view<char_type> fmt, basic_format_args<fmt_ctx_t> args);
		format_handler(out_it_t output, basic_string_view<char_type> fmt, basic_format_args<fmt_ctx_t> args, const std::locale& l);
		constexpr fmt_it_t do_raw(fmt_it_t start, fmt_it_t fin);
		constexpr fmt_it_t do_format_spec(fmt_it_t start, fmt_it_t fin, std::size_t id);
		constexpr fmt_it_t do_empty_format(fmt_it_t start, fmt_it_t last, std::size_t id);

		constexpr std::size_t on_arg_id(void);
		constexpr std::size_t on_arg_id(std::size_t i);
		constexpr std::size_t on_arg_id(fmt_it_t start, fmt_it_t last);
	};
	template<class Char>
	format_handler(output_iterator_t<Char>, basic_string_view<Char>, basic_format_args<fmt_context_t<Char>>) -> format_handler<Char>;

	//Constant-evaluated version of format_handler. Does not perform formatting but will complete a parsing
	//pass during compile
	template<class Char, class... Args>
	class format_checker
	{
	public:
		using char_type = Char;
		using parse_ctx_t = parse_context_t<char_type>;
		using fmt_it_t    = typename parse_ctx_t::iterator;

		parse_ctx_t parse_ctx;

		consteval format_checker(void) = default;
		consteval format_checker(basic_string_view<char_type> fmt, std::size_t numargs = 0);
		constexpr fmt_it_t do_raw(fmt_it_t, fmt_it_t last);
		constexpr fmt_it_t do_format_spec(fmt_it_t start, fmt_it_t last, std::size_t id);
		constexpr void do_empty_format(fmt_it_t, fmt_it_t, std::size_t);

		constexpr std::size_t on_arg_id(void);
		constexpr std::size_t on_arg_id(std::size_t i);
		constexpr std::size_t on_arg_id(fmt_it_t start, fmt_it_t last);

	private:
		template<std::size_t I, class FArg, class... FArgs>
		constexpr fmt_it_t do_format_spec_impl(fmt_it_t start, fmt_it_t last, std::size_t id);
	};

}

#endif
