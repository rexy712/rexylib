/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_PARSE_HPP
#define REXY_DETAIL_FORMAT_PARSE_HPP

#include "../../string_view.hpp"
#include <cstddef> //size_t

namespace rexy::fmt::detail::parse{

	//A few basic string checking functions
	template<class Char>
	constexpr auto find_first_of_it(Char v,
	                                typename basic_string_view<Char>::const_iterator start_it,
	                                typename basic_string_view<Char>::const_iterator last_it);
	template<class Char>
	constexpr auto find_first_of_it(const basic_string_view<Char>& str, Char v);
	template<class It>
	constexpr bool check_duplicate_char(It start, It end);
	template<class Char>
	constexpr bool is_a_number(Char c);
	template<class It>
	constexpr int to_integer(It start, It end);
	template<class It>
	constexpr It find_valid_index(It start, It end);

	//Used with 'visit_format_arg' as the 'fun' arg. Gets the value of the argument as a 'long long'
	//if the argument is of integral type. Throws an error otherwise
	struct dynamic_integer_retriever{
		template<class T>
		constexpr long long operator()(T&& t);
	};

	//Used during call to 'perform_standard_nested_replacement_field' as the 'Adapter' type.
	//Calls 'on_dynamic_width' of 'specs'
	template<class Specs>
	struct dynamic_width_adapter{
		Specs& specs;

		constexpr decltype(auto) operator()(void);
		constexpr decltype(auto) operator()(int i);
		template<class It>
		constexpr decltype(auto) operator()(It start, It last);
	};
	//Used during call to 'perform_standard_nested_replacement_field' as the 'Adapter' type.
	//Calls 'on_dynamic_precision' of 'specs'
	template<class Specs>
	struct dynamic_precision_adapter{
		Specs& specs;

		constexpr decltype(auto) operator()(void);
		constexpr decltype(auto) operator()(int i);
		template<class It>
		constexpr decltype(auto) operator()(It start, It last);
	};
	template<class Handler>
	struct dynamic_index_adapter{
		Handler& handler;
		std::size_t& id;
		constexpr decltype(auto) operator()(void);
		constexpr decltype(auto) operator()(int i);
		template<class It>
		constexpr decltype(auto) operator()(It start, It last);
	};

	//Standalone functions for parsing standard format fields
	template<class It, class FormatSpecs>
	constexpr It perform_standard_fill_align_option(It start, It last, FormatSpecs&& specs);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_sign_option(It start, It last, FormatSpecs&& specs);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_alt_form_option(It start, It last, FormatSpecs&& specs);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_zero_fill_option(It start, It last, FormatSpecs&& specs);
	template<class It, class Adapter>
	constexpr It perform_standard_nested_replacement_field(It start, It last, Adapter&& func);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_width_option(It start, It last, FormatSpecs&& specs);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_precision_option(It start, It last, FormatSpecs&& specs);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_locale_option(It start, It last, FormatSpecs&& specs);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_type_option(It start, It last, FormatSpecs&& specs);
	template<class It, class FormatSpecs>
	constexpr It perform_standard_parse(It start, It last, FormatSpecs&& specs);

	//Standalone functions for parsing the entire format string
	template<class Handler, class It>
	constexpr It perform_format_index_spec(Handler&& handler, It start, It last, std::size_t& id);
	template<class Handler, class It>
	constexpr It perform_empty_format_field_parse(Handler&& handler, It start, It last);
	template<class Handler, class It>
	constexpr It perform_format_field_parse(Handler&& handler, It start, It last);
	template<class Handler, class Char>
	constexpr void perform_parse(Handler&& handler, basic_string_view<Char> fmt);

}

#endif
