/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_PARSE_CONTEXT_HPP
#define REXY_DETAIL_FORMAT_PARSE_CONTEXT_HPP

#include <cstddef> //size_t

#include "../../string_view.hpp"

namespace rexy::fmt{

	//Holds the format string for use in calls to 'formatter<T>::parse'
	//Additionally keeps tally of the current argument index for automatic replacement indexing or
	//switches to manual indexing mode when encountering a manually indexed field. Throws error on mode switch.
	template<class Char>
	class basic_format_parse_context
	{
	public:
		using char_type = Char;
		using iterator = typename basic_string_view<Char>::const_iterator;
		using const_iterator = typename basic_string_view<Char>::const_iterator;

	public:
		basic_string_view<char_type> format;
		std::size_t arg_count;
		long long arg_index = 0;

	public:
		//fmt is the format specifier, num_args is 'sizeof...(Args)' where 'Args' is the parameter pack of the format call
		constexpr explicit basic_format_parse_context(basic_string_view<char_type> fmt, std::size_t num_args = 0)noexcept;
		basic_format_parse_context(const basic_format_parse_context&) = delete;
		basic_format_parse_context(basic_format_parse_context&&) = delete;
		basic_format_parse_context& operator=(basic_format_parse_context&&) = delete;
		basic_format_parse_context& operator=(const basic_format_parse_context&) = delete;

		constexpr const_iterator begin(void)const noexcept;
		constexpr const_iterator end(void)const noexcept;
		constexpr void advance_to(const_iterator it);
		constexpr std::size_t next_arg_id(void);
		constexpr void check_arg_id(std::size_t id);
	};

}

#endif
