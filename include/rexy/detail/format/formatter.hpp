/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_FORMATTER_HPP
#define REXY_DETAIL_FORMAT_FORMATTER_HPP

#include <cstddef> //std::size_t, nullptr_t
#include <variant> //monostate
#include <locale> //locale
#include <type_traits> //remove_cvref
#include <utility> //forward

#include "basic_types.hpp" //format_specs
#include "internal_types.hpp"
#include "standard_types.hpp" //basic_parse_context
#include "specs_handler.hpp" //format_specs_checker
#include "traits.hpp"

#include "../../string.hpp"
#include "../../string_view.hpp"
#include "../../allocator.hpp"

namespace rexy::fmt{

	namespace detail{

		namespace format{

			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(const void* t, OutIt out, const format_specs& specs, const std::locale& loc);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(const void* t, OutIt out);
			template<class Char, class OutIt>
				requires(!UTF8_String<Char>)
			constexpr OutIt perform_standard_format(const Char* c, OutIt out, const format_specs& specs, const std::locale& loc);
			template<UTF8_String Char, class OutIt>
				requires(UTF8_String<Char>)
			constexpr OutIt perform_standard_format(const Char* c, OutIt out, const format_specs& specs, const std::locale& loc);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(const Char* c, OutIt out);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(basic_string_view<std::type_identity_t<Char>> str, OutIt out, const format_specs& specs, const std::locale& loc);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(basic_string_view<std::type_identity_t<Char>> str, OutIt out);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(Char b, OutIt out, const format_specs& specs, const std::locale& loc);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(Char b, OutIt out);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(bool b, OutIt out, const format_specs& specs, const std::locale& loc);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(bool b, OutIt out);
			template<class Char, class OutIt, Integral T>
			constexpr OutIt perform_standard_format(T b, OutIt out, const format_specs& specs, const std::locale& loc);
			template<class Char, class OutIt, Arithmetic T>
			constexpr OutIt perform_standard_format(T b, OutIt out);
			template<class Char, class OutIt, Floating T>
			constexpr OutIt perform_standard_format(T f, OutIt out, const format_specs& specs, const std::locale& loc);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(std::monostate, OutIt out, const format_specs&, const std::locale&);
			template<class Char, class OutIt>
			constexpr OutIt perform_standard_format(std::monostate, OutIt out);

			template<class FmtCtx, class ParseCtx, class Specs>
			struct arg_formatter{
				FmtCtx& fmt_ctx;
				ParseCtx& parse_ctx;
				const Specs& specs;

				template<Handle<FmtCtx> T>
				constexpr void operator()(T&& t);
				template<class T>
				constexpr void operator()(T&& t);
			};
			template<class FmtCtx, class ParseCtx>
			struct empty_formatter{
				FmtCtx& fmt_ctx;
				ParseCtx& parse_ctx;

				template<Handle<FmtCtx> T>
				constexpr void operator()(T&& t);
				template<class T>
				constexpr void operator()(T&& t);
			};
		}


		template<class T, class Char>
		class formatter_base
		{
		public:
			using char_type = Char;
			using parse_ctx_t = basic_format_parse_context<char_type>;
			using fmt_ctx_t = detail::fmt_context_t<char_type>;
			using format_spec_t = detail::dynamic_format_specs<char_type>;
			using format_spec_handler_t = detail::format_specs_checker<detail::dynamic_format_specs_handler<parse_ctx_t>>;

		protected:
			format_spec_t specs;

		public:
			constexpr auto parse(basic_format_parse_context<Char>& ctx) -> decltype(ctx.begin());
			template<class U, class FormatContext>
			auto format(U&& t, FormatContext& ctx) -> decltype(ctx.out());
		};

	}

}

namespace rexy{
	template<class T, class Char = char>
	class formatter;

#define IMPLEMENT_STANDARD_FORMATTER(type) \
	template<fmt::detail::Formatter_Char C> \
	class formatter<type, C> : public fmt::detail::formatter_base<fmt::detail::stored_type_t<type,fmt::detail::fmt_context_t<C>>, C>{}

	IMPLEMENT_STANDARD_FORMATTER(int);
	IMPLEMENT_STANDARD_FORMATTER(unsigned int);
	IMPLEMENT_STANDARD_FORMATTER(long long);
	IMPLEMENT_STANDARD_FORMATTER(unsigned long long);
	IMPLEMENT_STANDARD_FORMATTER(bool);
	IMPLEMENT_STANDARD_FORMATTER(float);
	IMPLEMENT_STANDARD_FORMATTER(double);
	IMPLEMENT_STANDARD_FORMATTER(long double);
	IMPLEMENT_STANDARD_FORMATTER(std::nullptr_t);
	IMPLEMENT_STANDARD_FORMATTER(short);
	IMPLEMENT_STANDARD_FORMATTER(unsigned short);
	IMPLEMENT_STANDARD_FORMATTER(long);
	IMPLEMENT_STANDARD_FORMATTER(unsigned long);
	IMPLEMENT_STANDARD_FORMATTER(char);
	IMPLEMENT_STANDARD_FORMATTER(unsigned char);
	IMPLEMENT_STANDARD_FORMATTER(signed char);

	template<fmt::detail::Formatter_Char C>
	class formatter<C*,C> : public fmt::detail::formatter_base<const C*,C>{};

	template<fmt::detail::Formatter_Char C>
	class formatter<const C*,C> : public fmt::detail::formatter_base<const C*,C>{};

	template<fmt::detail::Formatter_Char C, std::size_t I>
	class formatter<const C[I],C> : public fmt::detail::formatter_base<const C*,C>{};

	template<fmt::detail::Formatter_Char C, REXY_ALLOCATOR_CONCEPT A>
	class formatter<basic_string<C,A>,C> : public fmt::detail::formatter_base<basic_string_view<C>,C>{};

	template<fmt::detail::Formatter_Char C>
	class formatter<basic_string_view<C>,C> : public fmt::detail::formatter_base<basic_string_view<C>,C>{};

	template<class T, fmt::detail::Formatter_Char C>
	class formatter<T*,C> : public fmt::detail::formatter_base<const void*,C>{};

	template<class T, fmt::detail::Formatter_Char C>
	class formatter<const T*,C> : public fmt::detail::formatter_base<const void*,C>{};


#undef IMPLEMENT_STANDARD_FORMATTER


}

#endif
