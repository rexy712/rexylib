/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_SPECS_HANDLER_HPP
#define REXY_DETAIL_FORMAT_SPECS_HANDLER_HPP

#include "../../compat/if_consteval.hpp"

namespace rexy::fmt::detail{

	//Stores standard format-spec information to use later in formatting
	template<class ParseCtx, class FmtCtx>
	struct format_specs_handler{
		ParseCtx& parse_ctx;
		FmtCtx& fmt_ctx;
		format_specs& specs;

		constexpr format_specs_handler(ParseCtx& pc, FmtCtx& fc, format_specs& s):
			parse_ctx(pc),
			fmt_ctx(fc),
			specs(s){}

		constexpr void on_width(int w);
		constexpr void on_dynamic_width(void);
		constexpr void on_dynamic_width(int index);
		template<class It>
		constexpr void on_dynamic_width(It first, It last);
		constexpr void on_precision(int p);
		constexpr void on_dynamic_precision(void);
		constexpr void on_dynamic_precision(int index);
		template<class It>
		constexpr void on_dynamic_precision(It first, It last);
		constexpr void on_locale(void);
		constexpr void on_type_option(int o = 0);
		constexpr void on_align(int al, int val);
		constexpr void on_sign(int s);
		constexpr void on_alt_form(void);
		constexpr void on_zero_fill(void);
	};

	//Stores standard format-spec information to use later in formatting. Stores indices of dynamic specifiers
	//to be retrieved later.
	template<class ParseCtx>
	struct dynamic_format_specs_handler{
		using specs_type = dynamic_format_specs<typename ParseCtx::char_type>;
		ParseCtx& parse_ctx;
		specs_type& specs;

		constexpr dynamic_format_specs_handler(ParseCtx& pc, specs_type& s);

		constexpr void on_width(int w);
		constexpr void on_dynamic_width(void);
		constexpr void on_dynamic_width(int index);
		template<class It>
		constexpr void on_dynamic_width(It first, It last);
		constexpr void on_precision(int p);
		constexpr void on_dynamic_precision(void);
		constexpr void on_dynamic_precision(int index);
		template<class It>
		constexpr void on_dynamic_precision(It first, It last);
		constexpr void on_locale(void);
		constexpr void on_type_option(int o = 0);
		constexpr void on_align(int al, int val);
		constexpr void on_sign(int s);
		constexpr void on_alt_form(void);
		constexpr void on_zero_fill(void);
	};
	//Used during compile time only. Checks dynamic specifiers given types pack as template parameters.
	template<class ParseCtx, class... Args>
	struct cx_format_specs_handler : public dynamic_format_specs_handler<ParseCtx>{
		using char_type = typename ParseCtx::char_type;

		constexpr cx_format_specs_handler(ParseCtx& pc, dynamic_format_specs<char_type>& s):
			dynamic_format_specs_handler<ParseCtx>(pc, s){
				REXY_if_not_consteval{
					throw 0;
				}
			}

		constexpr void on_dynamic_width(void);
		constexpr void on_dynamic_width(int index);
		template<class It>
		constexpr void on_dynamic_width(It first, It last);
		constexpr void on_dynamic_precision(void);
		constexpr void on_dynamic_precision(int index);
		template<class It>
		constexpr void on_dynamic_precision(It first, It last);
	};

	template<class Handler>
	struct format_specs_checker : public Handler{
		storage_type arg_type = storage_type::none_t;
		bool requires_arithmetic_presentation = false;

		constexpr format_specs_checker(const Handler& h, storage_type type);

		constexpr void on_precision(int p);
		constexpr void on_dynamic_precision(void);
		constexpr void on_dynamic_precision(int index);
		template<class It>
		constexpr void on_dynamic_precision(It first, It last);
		constexpr void on_locale(void);
		constexpr void on_sign(int s);
		constexpr void on_alt_form(void);
		constexpr void on_zero_fill(void);
		constexpr void on_type_option(int type = 0);
	private:
		constexpr void check_precision(void);
		constexpr void check_arithmetic_type(void);
	};

}

#endif
