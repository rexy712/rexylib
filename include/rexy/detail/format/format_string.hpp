/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_FORMAT_STRING_HPP
#define REXY_DETAIL_FORMAT_FORMAT_STRING_HPP

#include "../../string_view.hpp"

namespace rexy::fmt::detail{

	//Class accepted as an argument to 'format' calls. Will call a constant-evaluated parse of the format string
	//to check for correctness.
	template<class Char, class... Args>
	class basic_format_string
	{
	public:
		basic_string_view<Char> str;

		consteval basic_format_string(const Char* f);
		consteval basic_format_string(basic_string_view<Char> f);
	};

}

#endif
