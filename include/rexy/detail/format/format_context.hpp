/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_FORMAT_CONTEXT_HPP
#define REXY_DETAIL_FORMAT_FORMAT_CONTEXT_HPP

#include <type_traits> //remove_cvref, declval, integral_constant, void_t
#include <cstddef> //size_t
#include <locale> //locale

#include "standard_types.hpp" //basic_format_args, formatter

namespace rexy::fmt{

	namespace detail{

		//type trait used for properly cv-qualifying the argument to a call to formatter<type>::format
		template<class T, class Context, class = void>
		struct is_formatter_const : public std::false_type{};
		template<class T, class Context>
		struct is_formatter_const<T,Context,std::void_t<
			decltype(std::declval<typename Context::template formatter_type<std::remove_cvref_t<T>>>().format(std::declval<const T&>(), std::declval<Context>()))
		>> : public std::true_type{};
		template<class T, class Context>
		concept has_formatter = requires(typename Context::template formatter_type<std::remove_cvref_t<T>> fmter, T& t, Context& ctx){
			fmter.format(t, ctx);
		};
		template<class T, class Context>
		concept has_const_formatter = has_formatter<const std::remove_cvref_t<T>, Context>;

	}

	//Holds the arguments passed to calls to 'format' or 'vformat' in type erasing structure.
	//Additionally holds the iterator where output is written.
	template<class OutIt, class Char>
	class basic_format_context
	{
	public:
		using iterator = OutIt;
		using char_type = Char;
		template<class T>
		using formatter_type = formatter<T, Char>;

	private:
		basic_format_args<basic_format_context> m_fmt_args;
		iterator m_outit;
		std::locale m_locale;

	public:
		basic_format_context(basic_format_args<basic_format_context> fmt_args, iterator outit);
		basic_format_context(basic_format_args<basic_format_context> fmt_args, iterator outit, const std::locale& l);
		basic_format_arg<basic_format_context> arg(std::size_t id)const;
		basic_format_arg<basic_format_context> arg(const char_type* first, const char_type* last)const;
		std::size_t arg_index(const char_type* first, const char_type* last)const;
		std::locale locale(void);
		iterator out(void);
		void advance_to(iterator it);
	};

}

#endif
