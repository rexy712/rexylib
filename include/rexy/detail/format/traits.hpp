/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_TRAITS_HPP
#define REXY_DETAIL_FORMAT_TRAITS_HPP

#include <type_traits> //is_convertible, is_floating_point, integral_constant, is_same, remove_cvref
#include <concepts> //integral, floating_point
#include <cstddef> //size_t

#include "standard_types.hpp"
#include "storage.hpp"
#include "named_args.hpp"
#include "internal_types.hpp"

namespace rexy::fmt::detail{

	template<class T>
	struct valid_dynamic_index{
		static constexpr bool value = std::is_convertible_v<T,long long> && !std::is_floating_point_v<T>;
	};
	template<class T>
	static constexpr bool valid_dynamic_index_v = valid_dynamic_index<T>::value;

	template<class T>
	struct is_char_or_bool : public std::false_type{};
	template<>
	struct is_char_or_bool<bool> : public std::true_type{};
	template<>
	struct is_char_or_bool<char> : public std::true_type{};
	template<>
	struct is_char_or_bool<wchar_t> : public std::true_type{};

	template<class T>
	concept Integral = std::integral<T> && !is_char_or_bool<T>::value;
	template<class T>
	concept Bool = std::is_same_v<std::remove_cvref_t<T>,bool>;
	template<class T>
	concept Floating = std::floating_point<T>;
	template<class T>
	concept Arithmetic = Integral<T> || Floating<T>;
	template<class T, class FmtCtx>
	concept Handle = std::is_same_v<std::remove_cvref_t<T>,typename basic_format_arg<FmtCtx>::handle>;
	template<class T>
	concept Formatter_Char = std::is_same_v<T,char> || std::is_same_v<T,wchar_t>;
	template<class T, class FmtCtx>
	struct is_handle{
		static constexpr bool value = Handle<T,FmtCtx>;
	};
	template<class T, class FmtCtx>
	static constexpr bool is_handle_v = is_handle<T,FmtCtx>::value;

	template<class T>
	struct extract_char_type_from_context;
	template<class OutIt, class Char>
	struct extract_char_type_from_context<basic_format_context<OutIt,Char>>{
		using type = Char;
	};
	template<class T>
	using extract_char_type_from_context_t = typename extract_char_type_from_context<T>::type;


	template<std::size_t I, class FmtCtx, class Arg, class... Args>
	constexpr bool is_dynamic_integer_impl(int index){
		using mapped_t = stored_type_t<Arg,FmtCtx>;
		if(index == I){
			return valid_dynamic_index_v<mapped_t>;
		}
		if constexpr(sizeof...(Args) > 0){
			return is_dynamic_integer_impl<I+1,FmtCtx,Args...>(index);
		}
		return false;
	}
	template<class Char, class... Args>
	constexpr bool is_dynamic_integer(int index){
		using fmt_ctx_t = fmt_context_t<Char>;
		if constexpr(sizeof...(Args) == 0){
			return false;
		}else{
			return is_dynamic_integer_impl<0,fmt_ctx_t,Args...>(index);
		}
	}

	static constexpr unsigned char utf_test[] = "\u00B5";
	constexpr bool is_utf8(void){
		using uchar = unsigned char;
		return (sizeof(utf_test) == 3) && (uchar(utf_test[0]) == 0xC2) && (uchar(utf_test[1]) == 0xB5);
	}
	static constexpr wchar_t wutf_test[] = L"\U00010437";
	constexpr bool is_wutf16(void){
		return (sizeof(wutf_test) / sizeof(wutf_test[0]) == 3 &&
		        wutf_test[0] == 0xD801 && wutf_test[1] == 0xDC37);
	}
	constexpr bool is_wutf32(void){
		return (sizeof(wutf_test) / sizeof(wutf_test[0]) == 2 &&
		        wutf_test[0] == 0x00010437);
	}

	template<class T>
	struct is_utf8_encoded_string_type : public std::false_type{};
	template<>
	struct is_utf8_encoded_string_type<char8_t> : public std::true_type{};
	template<>
	struct is_utf8_encoded_string_type<char> : public std::integral_constant<bool,is_utf8()>{};
	template<class T>
	static constexpr bool is_utf8_encoded_string_type_v = is_utf8_encoded_string_type<T>::value;

	template<class T>
	struct is_utf16_encoded_string_type : public std::false_type{};
	template<>
	struct is_utf16_encoded_string_type<char16_t> : public std::true_type{};
	template<>
	struct is_utf16_encoded_string_type<wchar_t> : public std::integral_constant<bool,is_wutf16()>{};
	template<class T>
	static constexpr bool is_utf16_encoded_string_type_v = is_utf16_encoded_string_type<T>::value;

	template<class T>
	struct is_utf32_encoded_string_type : public std::false_type{};
	template<>
	struct is_utf32_encoded_string_type<char32_t> : public std::true_type{};
	template<>
	struct is_utf32_encoded_string_type<wchar_t> : public std::integral_constant<bool,is_wutf32()>{};
	template<class T>
	static constexpr bool is_utf32_encoded_string_type_v = is_utf32_encoded_string_type<T>::value;

	template<class T>
	concept UTF8_String = is_utf8_encoded_string_type_v<T>;
	template<class T>
	concept UTF16_String = is_utf16_encoded_string_type_v<T>;
	template<class T>
	concept UTF32_String = is_utf32_encoded_string_type_v<T>;

}

#endif
