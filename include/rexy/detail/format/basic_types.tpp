/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_BASIC_TYPES_TPP
#define REXY_DETAIL_FORMAT_BASIC_TYPES_TPP

#include "basic_types.hpp"

#include "format_args.hpp"

#include <cstddef> //size_t

namespace rexy::fmt::detail{

	template<class FormatCtx>
	constexpr void normalize_dynamic_format_specs(FormatCtx& ctx, dynamic_format_specs<typename FormatCtx::char_type>& specs){
		std::size_t index = 0;
		if(specs.width_type == dyn_type::index){
			index = specs.dyn_width.index;
		}else{
			const auto& name = specs.dyn_width.name;
			index = ctx.arg_index(name.name, name.name + name.length);
		}
		specs.width = ((index != invalid_arg_index)
										 ? visit_format_arg(parse::dynamic_integer_retriever{}, ctx.arg(index))
										 : specs.width);
		if(specs.precision_type == dyn_type::index){
			index = specs.dyn_precision.index;
		}else{
			const auto& name = specs.dyn_precision.name;
			index = ctx.arg_index(name.name, name.name + name.length);
		}
		specs.precision = ((index != invalid_arg_index)
												 ? visit_format_arg(parse::dynamic_integer_retriever{}, ctx.arg(index))
												 : specs.precision);
	}

}

#endif
