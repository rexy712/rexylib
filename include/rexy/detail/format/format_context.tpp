/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_FORMAT_CONTEXT_TPP
#define REXY_DETAIL_FORMAT_FORMAT_CONTEXT_TPP

#include "format_context.hpp"

#include "format_args.hpp"

#include <utility> //move
#include <cstddef> //size_t
#include <locale> //locale

namespace rexy::fmt{

	///////////////////////////////basic_format_context////////////////////////////////
	template<class OutIt, class Char>
	basic_format_context<OutIt,Char>::basic_format_context(basic_format_args<basic_format_context> fmt_args, iterator outit):
		m_fmt_args(fmt_args),
		m_outit(outit){}
	template<class OutIt, class Char>
	basic_format_context<OutIt,Char>::basic_format_context(basic_format_args<basic_format_context> fmt_args, iterator outit, const std::locale& l):
		m_fmt_args(fmt_args),
		m_outit(outit),
		m_locale(l){}
	template<class OutIt, class Char>
	auto basic_format_context<OutIt,Char>::arg(std::size_t id)const -> basic_format_arg<basic_format_context>{
		return m_fmt_args.get(id);
	}
	template<class OutIt, class Char>
	auto basic_format_context<OutIt,Char>::arg(const char_type* first, const char_type* last)const -> basic_format_arg<basic_format_context>{
		return m_fmt_args.get(first, last);
	}
	template<class OutIt, class Char>
	std::size_t basic_format_context<OutIt,Char>::arg_index(const char_type* first, const char_type* last)const{
		return m_fmt_args.get_index(first, last);
	}

	template<class OutIt, class Char>
	std::locale basic_format_context<OutIt,Char>::locale(void){
		return m_locale;
	}
	template<class OutIt, class Char>
	auto basic_format_context<OutIt,Char>::out(void) -> iterator{
		return std::move(m_outit);
	}
	template<class OutIt, class Char>
	void basic_format_context<OutIt,Char>::advance_to(iterator it){
		m_outit = std::move(it);
	}

}

#endif
