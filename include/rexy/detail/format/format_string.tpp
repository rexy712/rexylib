/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_FORMAT_STRING_TPP
#define REXY_DETAIL_FORMAT_FORMAT_STRING_TPP

#include "format_string.hpp"

#include "context_handler.hpp"
#include "parse.hpp"

#include "../../string_view.hpp"

namespace rexy::fmt::detail{

	template<class Char, class... Args>
	consteval basic_format_string<Char,Args...>::basic_format_string(const Char* f):
		basic_format_string(basic_string_view<Char>{f}){}
	template<class Char, class... Args>
	consteval basic_format_string<Char,Args...>::basic_format_string(basic_string_view<Char> f):
		str(f)
	{
		parse::perform_parse(format_checker<Char, Args...>{f, sizeof...(Args)}, str);
	}

}

#endif
