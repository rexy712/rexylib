/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_PARSE_CONTEXT_TPP
#define REXY_DETAIL_FORMAT_PARSE_CONTEXT_TPP

#include "../../compat/if_consteval.hpp"

#include "parse_context.hpp"

#include "format_error.hpp"

#include <cstddef> //size_t

namespace rexy::fmt{

	/////////////////////////////basic_format_parse_context//////////////////////////////
	template<class Char>
	constexpr basic_format_parse_context<Char>::basic_format_parse_context(basic_string_view<char_type> fmt, std::size_t num_args)noexcept:
		format(fmt),
		arg_count(num_args){}

	template<class Char>
	constexpr auto basic_format_parse_context<Char>::begin(void)const noexcept -> const_iterator{
		return format.begin();
	}
	template<class Char>
	constexpr auto basic_format_parse_context<Char>::end(void)const noexcept -> const_iterator{
		return format.end();
	}
	template<class Char>
	constexpr void basic_format_parse_context<Char>::advance_to(const_iterator it){
		const std::size_t diff = it - format.begin();
		format.remove_prefix(diff);
	}
	template<class Char>
	constexpr std::size_t basic_format_parse_context<Char>::next_arg_id(void){
		if(arg_index < 0){
			//error, already in manual mode
			REXY_THROW_FORMAT_ERROR("Invalid indexing mode switch");
		}
		REXY_if_consteval{
			if(arg_index >= arg_count){
				REXY_THROW_FORMAT_ERROR("Missing argument");
			}
		}
		return arg_index++;
	}
	template<class Char>
	constexpr void basic_format_parse_context<Char>::check_arg_id(std::size_t id){
		if(arg_index > 0){
			//error, already in automatic mode
			REXY_THROW_FORMAT_ERROR("Invalid indexing mode switch");
		}
		REXY_if_consteval{
			if(id >= arg_count){
				REXY_THROW_FORMAT_ERROR("Missing argument");
			}
		}
		arg_index = detail::invalid_arg_index;
	}

}

#endif
