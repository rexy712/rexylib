/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_STORAGE_HPP
#define REXY_DETAIL_FORMAT_STORAGE_HPP

#include <type_traits> //declval

namespace rexy::fmt::detail{

	//tag for use in basic_format_arg(s)
	enum class storage_type{
		none_t = 0,

		bool_t,
		char_t,
		int_t,
		uint_t,
		long_long_t,
		ulong_long_t,
		float_t,
		double_t,
		long_double_t,

		char_ptr_t,
		string_t,
		ptr_t,

		custom_t
	};
	constexpr bool is_integral_storage_type(storage_type t){
		return t >= storage_type::bool_t && t <= storage_type::long_double_t;
	}
	constexpr bool is_integer_storage_type(storage_type t){
		return t >= storage_type::bool_t && t <= storage_type::ulong_long_t;
	}
	constexpr bool is_floating_storage_type(storage_type t){
		return t >= storage_type::float_t && t <= storage_type::long_double_t;
	}
	constexpr bool is_string_storage_type(storage_type t){
		return t == storage_type::string_t || t == storage_type::char_ptr_t;
	}

	//Mappings from actual type to the type used in basic_format_arg(s)
	template<class T, class Char>
	consteval storage_type map_to_storage_enum(void);
	template<class T, class Char>
	static constexpr storage_type map_to_storage_enum_v = map_to_storage_enum<T,Char>();
	template<class Context>
	struct map_to_stored_type_helper;
  template<class T, class Context>
  using stored_type_t = decltype(std::declval<map_to_stored_type_helper<Context>>()(std::declval<T>()));

}

#endif
