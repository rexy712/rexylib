/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_ARG_STORE_TPP
#define REXY_DETAIL_FORMAT_ARG_STORE_TPP

#include "arg_store.hpp"

#include "storage.hpp"
#include "basic_types.hpp"

#include "../../utility.hpp" //memcpy

#include <utility> //forward
#include <memory> //addressof
#include <cstddef> //size_t

namespace rexy::fmt::detail{

	///////////////////////basic_format_arg_store////////////////////////
	template<class Context, class... Args>
	basic_format_arg_store<Context,Args...>::basic_format_arg_store(Args&&... args){
		store_args(std::forward<Args>(args)...);
	}
	template<class Context, class... Args>
	template<class Arg>
	void basic_format_arg_store<Context,Args...>::store_arg(std::size_t argnum, Arg&& arg){
		using stored_type = stored_type_t<Arg,Context>;
		arg_info& ai = packed_data.info()[argnum];
		ai.type = map_to_storage_enum_v<stored_type,typename Context::char_type>;

		//convert to one of the storable types
		stored_type st{std::forward<Arg>(arg)};

		//save to the array of data
		rexy::memcpy(packed_data.data()+ai.offset, std::addressof(st), sizeof(st));

		//setup next entry's offset
		if(argnum+1 < num_args){
			packed_data.info()[argnum+1].offset = ai.offset + sizeof(st);
		}
	}
	template<class Context, class... Args>
	template<NamedArg Arg>
	void basic_format_arg_store<Context,Args...>::store_arg(std::size_t argnum, Arg&& arg){
		using stored_type = stored_type_t<Arg,Context>;
		arg_info& ai = packed_data.info()[argnum];
		ai.type = map_to_storage_enum_v<stored_type,typename Context::char_type>;
		ai.named = true;
		const std::size_t name_size = sizeof(format_string_view<char_type>);

		//convert to one of the storable types
		stored_type st{std::forward<Arg>(arg).value};
		format_string_view<char_type> name{arg.name.c_str(), arg.name.length()};

		//save to the array of data
		rexy::memcpy(packed_data.data()+ai.offset, std::addressof(name), name_size);
		rexy::memcpy(packed_data.data()+ai.offset+name_size, std::addressof(st), sizeof(st));

		//setup next entry's offset
		if(argnum+1 < num_args){
			packed_data.info()[argnum+1].offset = ai.offset + sizeof(st) + name_size;
		}
	}
	template<class Context, class... Args>
	template<class... SArgs>
	void basic_format_arg_store<Context,Args...>::store_args(SArgs&&... args){
		std::size_t argnum = 0;
		(store_arg(argnum++, std::forward<SArgs>(args)), ...);
	}

}

#endif
