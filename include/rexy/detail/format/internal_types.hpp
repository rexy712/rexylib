/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DETAIL_FORMAT_INTERNAL_TYPES_HPP
#define REXY_DETAIL_FORMAT_INTERNAL_TYPES_HPP

#include "../../cx/string.hpp"
#include "standard_types.hpp"

#include <iterator> //back_insert_iterator
#include <type_traits> //type_identity

namespace rexy::fmt::detail{

	//forward declare implementation types
	template<class Char, class... Args>
	class basic_format_string;
	template<class Context, class... Args>
	class basic_format_arg_store;
	template<class Char>
	class format_output_buffer_base;
	template<class Char, class OutIt>
	class basic_format_output_buffer;
	struct format_specs;
	template<class Char>
	struct dynamic_format_specs;
	template<class ParseCtx, class... Args>
	struct checker_format_specs_handler;
	template<class ParseCtx, class FmtCtx>
	struct format_specs_handler;
	template<class ParseCtx>
	struct dynamic_format_specs_handler;
	template<class T, class Char>
	struct runtime_arg;
	template<class T, rexy::cx::string Name>
	struct static_arg;

	//aliases for easier access to both implementation and standard defined types
	template<class Char>
	using fmt_buffer_t = format_output_buffer_base<Char>;
	template<class Char>
	using output_iterator_t = std::back_insert_iterator<fmt_buffer_t<Char>>;
	template<class Char>
	using fmt_context_t = basic_format_context<output_iterator_t<Char>,Char>;
	template<class Char>
	using parse_context_t = basic_format_parse_context<Char>;
	template<class... Args>
	using format_arg_store = basic_format_arg_store<fmt_context_t<char>, Args...>;
	template<class... Args>
	using wformat_arg_store = basic_format_arg_store<fmt_context_t<wchar_t>, Args...>;
	template<class... Args>
	using format_string = basic_format_string<char,std::type_identity_t<Args>...>;
	template<class... Args>
	using wformat_string = basic_format_string<wchar_t, std::type_identity_t<Args>...>;
	template<class OutIt>
	using format_output_buffer = basic_format_output_buffer<char,OutIt>;
	template<class OutIt>
	using wformat_output_buffer = basic_format_output_buffer<wchar_t,OutIt>;

	template<class Char>
	using impl_format_args = basic_format_args<fmt_context_t<Char>>;
	template<class Char>
	using	format_arg = basic_format_arg<fmt_context_t<Char>>;


}

#endif
