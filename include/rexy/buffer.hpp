/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_BUFFER_HPP
#define REXY_BUFFER_HPP

#include "allocator.hpp"
#include "detail/hasallocator.hpp"
#include <cstddef> //size_t, ptrdiff_t
#include <iterator> //reverse_iterator

#include "storage_for.hpp"

#include "compat/standard.hpp"

namespace rexy{

	template<class T, class Allocator = allocator<T>>
	class buffer : protected detail::hasallocator<Allocator>
	{
	public:
		using value_type = T;
		using size_type = std::size_t;
		using difference_type = ptrdiff_t;
		using pointer = T*;
		using const_pointer = const T*;
		using reference = T&;
		using const_reference = const T&;
		using allocator_type = Allocator;
		using iterator = T*;
		using const_iterator = const T*;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

	protected:
		pointer m_data = nullptr;
		size_type m_cap = 0;
		size_type m_size = 0;

	public:
		constexpr buffer(void);
		REXY_CPP20_CONSTEXPR buffer(const_pointer data, size_type length)noexcept(is_nothrow_allocator_v<Allocator>);
		template<class Iter>
		REXY_CPP20_CONSTEXPR buffer(const Iter& start, const Iter& last);
		REXY_CPP20_CONSTEXPR buffer(size_type cap)noexcept(is_nothrow_allocator_v<Allocator>);
		REXY_CPP20_CONSTEXPR buffer(const buffer& b)noexcept(is_nothrow_allocator_v<Allocator>);
		constexpr buffer(buffer&& b)noexcept;
		REXY_CPP20_CONSTEXPR ~buffer(void)noexcept(is_nothrow_allocator_v<Allocator>);

		REXY_CPP20_CONSTEXPR buffer& operator=(const buffer& b)
			noexcept(is_nothrow_allocator_v<Allocator>);
		constexpr buffer& operator=(buffer&& b)noexcept;

		constexpr pointer data(void);
		constexpr const_pointer data(void)const;
		REXY_CPP20_CONSTEXPR void resize(size_type new_cap);
		constexpr void set_size(size_type size);
		constexpr size_type cap(void)const;
		constexpr const size_type& size(void)const;
		constexpr size_type& size(void);

		constexpr pointer release(void);

		constexpr reference operator[](size_type i);
		constexpr const_reference operator[](size_type i)const;
		constexpr reference at(size_type i);
		constexpr const_reference at(size_type i)const;

		constexpr iterator begin(void);
		constexpr const_iterator begin(void)const;
		constexpr const_iterator cbegin(void)const;
		constexpr iterator end(void);
		constexpr const_iterator end(void)const;
		constexpr const_iterator cend(void)const;
		constexpr reverse_iterator rbegin(void);
		constexpr const_reverse_iterator rbegin(void)const;
		constexpr reverse_iterator rend(void);
		constexpr const_reverse_iterator rend(void)const;
		constexpr const_reverse_iterator crbegin(void)const;
		constexpr const_reverse_iterator crend(void)const;

		REXY_CPP20_CONSTEXPR void clear(void);

		REXY_CPP20_CONSTEXPR void append(const_pointer p, size_type len);
	};

}

#include "buffer.tpp"

#endif
