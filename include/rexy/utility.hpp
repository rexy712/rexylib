/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_UTILITY_HPP
#define REXY_UTILITY_HPP

#include <utility> //forward, move
#include <cstddef> //size_t
#include <type_traits> //too many to enumerate
#include <string> //char_traits

#include "compat/if_consteval.hpp"
#include "rexy.hpp"
#include "storage_for.hpp"

#ifdef REXY_if_consteval
	#include <cstring> //strlen, strcmp, memcpy
	#include <cwchar> //wcslen
#endif

namespace rexy{

	namespace{
		template<class T>
		constexpr void swap(T& l, T& r)
			noexcept(std::is_nothrow_move_assignable<T>::value &&
			         std::is_nothrow_move_constructible<T>::value)
		{
			T tmp = std::move(l);
			l = std::move(r);
			r = std::move(tmp);
		}
		template<class Iter1, class Iter2>
		constexpr Iter2 swap_ranges(Iter1 start1, Iter1 end1, Iter2 start2)
			noexcept(noexcept(swap(*start1, *start2)))
		{
			while(start1 != end1){
				swap(start1++, start2++);
			}
			return start2;
		}
		template<class T, std::size_t N>
		constexpr void swap(T (&l)[N], T (&r)[N])
			noexcept(noexcept(swap(*l, *r)))
		{
			swap_ranges(l, l+N, r);
		}

		template<class T, class U = T>
		constexpr T exchange(T& l, U&& r)
			noexcept(std::is_nothrow_assignable<T,U&&>::value &&
			         std::is_nothrow_move_assignable<T>::value)
		{
			T old = std::move(l);
			l = std::forward<U>(r);
			return old;
		}

		template<class T>
		constexpr const T& min(const T& l, const T& r)noexcept{
			return l < r ? l : r;
		}
		template<class T, class Compare>
		constexpr const T& min(const T& l, const T& r, Compare cmp)
			noexcept(std::is_nothrow_invocable<Compare,const T&,const T&>::value)
		{
			return cmp(l, r) ? l : r;
		}
		template<class T>
		constexpr const T& max(const T& l, const T& r)noexcept{
			return l > r ? l : r;
		}
		template<class T, class Compare>
		constexpr const T& max(const T& l, const T& r, Compare cmp)
			noexcept(std::is_nothrow_invocable<Compare,const T&,const T&>::value)
		{
			return cmp(l, r) ? l : r;
		}
		template<class T>
		constexpr T abs(const T& val){
			return val > 0 ? val : -val;
		}

		template<class T>
		constexpr std::size_t strlen(const T* c)noexcept{
			return std::char_traits<T>::length(c);
		}

#ifdef REXY_if_consteval
		template<class T>
		constexpr int strcmp(const T* l, const T* r)noexcept{
			REXY_if_not_consteval{
				if constexpr(std::is_same_v<std::remove_cvref_t<T>,char>){
					return std::strcmp(l, r);
				}else if constexpr(std::is_same_v<std::remove_cvref_t<T>,wchar_t>){
					return std::wcscmp(l, r);
				}
			}
			for(;*l == *r && *l;++l, ++r);
			return *l - *r;
		}
		template<class T>
		constexpr int strncmp(const T* l, const T* r, std::size_t max)noexcept{
			REXY_if_not_consteval{
				if constexpr(std::is_same_v<std::remove_cvref_t<T>,char>){
					return std::strncmp(l, r, max);
				}else if constexpr (std::is_same_v<std::remove_cvref_t<T>,wchar_t>){
					return std::wcsncmp(l, r, max);
				}
			}
			for(std::size_t i = 1;*l == *r && *l && i < max;++i, ++l, ++r);
			return *l - *r;
		}
		template<class T, class Compare>
		constexpr int strncmp(const T* l, const T* r, std::size_t max, Compare cmp)noexcept{
			REXY_if_not_consteval{
				if constexpr(std::is_same_v<std::remove_cvref_t<T>,char>){
					return std::strncmp(l, r, max);
				}else if constexpr (std::is_same_v<std::remove_cvref_t<T>,wchar_t>){
					return std::wcsncmp(l, r, max);
				}
			}
			for(std::size_t i = 1;cmp(l, r) && *l && i < max;++i, ++l, ++r);
			return *l - *r;
		}
		template<class T, class Compare>
		constexpr int strcmp(const T* l, const T* r, Compare cmp)noexcept{
			for(;cmp(*l, *r) && *l;++l, ++r);
			return *l - *r;
		}
		constexpr void memcpy(void* l, const void* r, std::size_t n){
			REXY_if_not_consteval{
				std::memcpy(l, r, n);
			}else{
				char* ld = static_cast<char*>(l);
				const char* rd = static_cast<const char*>(r);
				for(std::size_t i = 0;i < n;++i){
					ld[i] = rd[i];
				}
			}
		}
	}
#else // REXY_if_consteval
		template<class T>
		constexpr int strcmp(const T* l, const T* r)noexcept{
			for(;*l == *r && *l;++l, ++r);
			return *l - *r;
		}
		template<class T, class Compare>
		constexpr int strcmp(const T* l, const T* r, Compare cmp)noexcept{
			for(;cmp(*l, *r) && *l;++l, ++r);
			return *l - *r;
		}
		template<class T>
		constexpr int strncmp(const T* l, const T* r, std::size_t max)noexcept{
			for(std::size_t i = 0;*l == *r && *l && i < max;++i, ++l, ++r);
			return *l - *r;
		}
		constexpr void memcpy(void* l, const void* r, std::size_t n){
			char* ld = static_cast<char*>(l);
			const char* rd = static_cast<const char*>(r);
			for(std::size_t i = 0;i < n;++i){
				ld[i] = rd[i];
			}
		}
	}
#endif // REXY_if_consteval


	template<class T>
	struct constant_iterator
	{
		rexy::storage_for<T> val;

		constexpr constant_iterator& operator++(void)noexcept{return *this;}
		constexpr constant_iterator operator++(int)noexcept{return *this;}
		constexpr T operator*(void)const noexcept{return val;}

		constexpr bool operator==(const constant_iterator& other)const{return val == other.val;}
		constexpr bool operator!=(const constant_iterator& other)const{return val != other.val;}

	};

	template<class T>
	constant_iterator(T&&) -> constant_iterator<std::remove_cvref_t<T>>;

	template<class T>
	struct sized_constant_iterator
	{
		rexy::storage_for<T> val;
		std::size_t count;

		constexpr sized_constant_iterator& operator++(void)noexcept{
			--count;
			return *this;
		}
		constexpr sized_constant_iterator operator++(int)noexcept{
			sized_constant_iterator other(*this);
			--count;
			return other;
		}
		constexpr const T& operator*(void)const noexcept{return val;}

		constexpr bool operator==(const sized_constant_iterator& other)const{
			return count == other.count;
		}
		constexpr bool operator!=(const sized_constant_iterator& other)const{
			return !(*this == other);
		}
	};
	template<class T>
	sized_constant_iterator(T&&, std::size_t) -> sized_constant_iterator<std::remove_cvref_t<T>>;


}

#endif
