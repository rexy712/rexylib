/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_EXPRESSION_HPP
#define REXY_EXPRESSION_HPP

#include <type_traits> //is_void, remove_reference, conditional
#include <utility> //forward

#include "rexy.hpp"

namespace rexy{

	template<class L, class R>
	class binary_expression
	{
		static_assert(!std::is_void_v<L>, "Left value of rexy::binary_expression cannot be void!");
		static_assert(!std::is_void_v<R>, "Right value of rexy::binary_expression cannot be void!");

	public:
		using left_type = std::conditional_t<std::is_rvalue_reference<L>::value,
		                                     std::remove_reference_t<L>,
		                                     L>;
		using right_type = std::conditional_t<std::is_rvalue_reference<R>::value,
		                                      std::remove_reference_t<R>,
		                                      R>;

		using left_reference = std::remove_reference_t<left_type>&;
		using left_const_reference = const std::remove_reference_t<left_type>&;
		using right_reference = std::remove_reference_t<right_type>&;
		using right_const_reference = const std::remove_reference_t<right_type>&;

	protected:
		left_type m_l;
		right_type m_r;

	public:
		template<class U, class V>
		constexpr binary_expression(U&& u, V&& v)noexcept:
			m_l(std::forward<U>(u)),
			m_r(std::forward<V>(v)){}
		constexpr binary_expression(const binary_expression&) = default;
		constexpr binary_expression(binary_expression&&) = default;

		constexpr binary_expression& operator=(const binary_expression&) = default;
		constexpr binary_expression& operator=(binary_expression&&) = default;

		constexpr left_reference left(void)noexcept{
			return m_l;
		}
		constexpr left_const_reference left(void)const noexcept{
			return m_l;
		}
		constexpr right_reference right(void)noexcept{
			return m_r;
		}
		constexpr right_const_reference right(void)const noexcept{
			return m_r;
		}

	};

	template<class L>
	class unary_expression
	{
		static_assert(!std::is_void_v<L>, "Value of rexy::unary_expression cannot be void!");

	public:
		using left_type = std::conditional_t<std::is_rvalue_reference<L>::value,
		                                     std::remove_reference_t<L>,
		                                     L>;
		using left_reference = std::remove_reference_t<left_type>&;
		using left_const_reference = const std::remove_reference_t<left_type>&;

		using value_type = left_type;
		using reference = left_reference;
		using const_reference = left_const_reference;

	protected:
		value_type m_l;

	public:
		template<class U>
		constexpr unary_expression(U&& u)noexcept:
			m_l(std::forward<U>(u)){}

		constexpr unary_expression(const unary_expression&)noexcept = default;
		constexpr unary_expression(unary_expression&&)noexcept = default;

		constexpr reference left(void)noexcept{
			return m_l;
		}
		constexpr const_reference left(void)const noexcept{
			return m_l;
		}

	};


}

#endif
