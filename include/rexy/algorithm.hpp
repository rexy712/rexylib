/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_ALGORITHM_HPP
#define REXY_ALGORITHM_HPP

#include "rexy.hpp"

#include "detail/algorithm.hpp"

namespace rexy{

	//Requires Iterators to be LegacyRandomAccessIterators
	//right is one past the end of the list
	template<class Iter, class Compare>
	constexpr void quicksort(Iter left, Iter right, const Compare& cmp)
		noexcept(noexcept(detail::qs_partition(left, right, cmp)));

	//Requires Iterators to be LegacyRandomAccessIterators
  template<class HIter, class NIter>
  constexpr HIter two_way_search(HIter hstart, HIter hend, NIter nstart, NIter nend);

	//searcher for use with generic search wrappers
  struct two_way_searcher{
  	template<class HIter, class NIter>
  	constexpr HIter operator()(HIter hstart, HIter hend, NIter nstart, NIter nend)const;
  };

}

#include "algorithm.tpp"

#endif
