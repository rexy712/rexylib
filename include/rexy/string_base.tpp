/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STRING_BASE_TPP
#define REXY_STRING_BASE_TPP

#include <utility> //move, etc
#include <type_traits> //is_nothrow_invokable, is_nothrow_constructible
#include <iterator> //reverse_iterator

#include "utility.hpp" //max, memcpy, strlen, constant_iterator
#include "detail/string_appender.hpp"
#include "algorithm.hpp"
#include "compat/to_address.hpp"
#include "string_view.hpp"

#include "compat/string_base.hpp"

namespace rexy{

	template<class Char>
	constexpr auto string_base<Char>::search(basic_string_view<value_type> sv)const noexcept -> const_iterator{
		if(sv.length() > length()){
			return cend();
		}
		return two_way_search(cbegin(), cend(), sv.cbegin(), sv.cend());
	}
	template<class Char>
	constexpr auto string_base<Char>::search(basic_string_view<value_type> sv)noexcept -> iterator{
		if(sv.length() > length()){
			return end();
		}
		return two_way_search(begin(), end(), sv.cbegin(), sv.cend());
	}
	template<class Char>
	constexpr auto string_base<Char>::search(const_pointer c)const noexcept -> const_iterator{
		const auto len = rexy::strlen(c);
		return two_way_search(cbegin(), cend(), c, c + len);
	}
	template<class Char>
	constexpr auto string_base<Char>::search(const_pointer c)noexcept -> iterator{
		const auto len = rexy::strlen(c);
		return two_way_search(begin(), end(), c, c + len);
	}
	template<class Char>
	template<class Searcher>
	constexpr auto string_base<Char>::search(const_pointer c, const Searcher& searcher)const noexcept(
			std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_pointer, const_pointer>) -> const_iterator
	{
		const auto len = rexy::strlen(c);
		return searcher(cbegin(), cend(), c, c + len);
	}
	template<class Char>
	template<class Searcher>
	constexpr auto string_base<Char>::search(const_pointer c, const Searcher& searcher)noexcept(
			std::is_nothrow_invocable_v<Searcher, iterator, iterator, const_pointer, const_pointer>) -> iterator
	{
		const auto len = rexy::strlen(c);
		return searcher(begin(), end(), c, c + len);
	}

	template<class Char>
	constexpr auto string_base<Char>::rsearch(basic_string_view<value_type> sv)const noexcept-> const_iterator{
		if(sv.length() > length()){
			return cend();
		}
		return two_way_search(crbegin(), crend(), sv.crbegin(), sv.crend()).base();
	}
	template<class Char>
	constexpr auto string_base<Char>::rsearch(basic_string_view<value_type> sv)noexcept -> iterator{
		if(sv.length() > length()){
			return end();
		}
		return two_way_search(rbegin(), rend(), sv.crbegin(), sv.crend()).base() - sv.length();
	}
	template<class Char>
	constexpr auto string_base<Char>::rsearch(const_pointer c)const noexcept -> const_iterator{
		const auto len = rexy::strlen(c);
		return two_way_search(crbegin(), crend(), std::reverse_iterator(c + len), std::reverse_iterator(c)).base() - len;
	}
	template<class Char>
	constexpr auto string_base<Char>::rsearch(const_pointer c)noexcept -> iterator{
		const auto len = rexy::strlen(c);
		return two_way_search(rbegin(), rend(), std::reverse_iterator(c + len), std::reverse_iterator(c)).base() - len;
	}
	template<class Char>
	template<class Searcher>
	constexpr auto string_base<Char>::rsearch(const_pointer c, const Searcher& searcher)const noexcept(
			std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_pointer, const_pointer>) -> const_iterator
	{
		const auto len = rexy::strlen(c);
		return searcher(crbegin(), crend(), std::reverse_iterator(c + len), std::reverse_iterator(c)).base() - len;
	}
	template<class Char>
	template<class Searcher>
	constexpr auto string_base<Char>::rsearch(const_pointer c, const Searcher& searcher)noexcept(
			std::is_nothrow_invocable_v<Searcher, iterator, iterator, const_pointer, const_pointer>) -> iterator
	{
		const auto len = rexy::strlen(c);
		return searcher(rbegin(), rend(), std::reverse_iterator(c + len), std::reverse_iterator(c)).base() - len;
	}

	template<class Char>
	constexpr bool string_base<Char>::starts_with(basic_string_view<value_type> sv)const noexcept{
		return basic_string_view<value_type>(data(), length()).starts_with(sv);
	}
	template<class Char>
	constexpr bool string_base<Char>::starts_with(value_type v)const noexcept{
		return front() == v;
	}
	template<class Char>
	constexpr bool string_base<Char>::starts_with(const_pointer str)const noexcept{
		return starts_with(basic_string_view<value_type>(str));
	}
	template<class Char>
	constexpr bool string_base<Char>::ends_with(basic_string_view<value_type> sv)const noexcept{
		return basic_string_view<value_type>(data(), length()).ends_with(sv);
	}
	template<class Char>
	constexpr bool string_base<Char>::ends_with(value_type v)const noexcept{
		return back() == v;
	}
	template<class Char>
	constexpr bool string_base<Char>::ends_with(const_pointer str)const noexcept{
		return ends_with(basic_string_view<value_type>(str));
	}

	template<class Char>
	constexpr bool string_base<Char>::contains(basic_string_view<value_type> sv)const noexcept{
		return basic_string_view<value_type>(data(), length()).contains(sv);
	}
	template<class Char>
	constexpr bool string_base<Char>::contains(value_type v)const noexcept{
		return basic_string_view<value_type>(data(), length()).contains(v);
	}
	template<class Char>
	constexpr bool string_base<Char>::contains(const_pointer str)const noexcept{
		return contains(basic_string_view<value_type>(str));
	}

	template<class Char>
	constexpr auto string_base<Char>::find_first_of(value_type v, size_type start)const noexcept-> size_type{
		return rexy::find_first_of(*this, &v, start, 1);
	}
	template<class Char>
	constexpr auto string_base<Char>::find_first_of(const_pointer c, size_type start)const noexcept -> size_type{
		return rexy::find_first_of(*this, c, start, rexy::strlen(c));
	}
	template<class Char>
	constexpr auto string_base<Char>::find_first_of(const_pointer c, size_type start, size_type size)const noexcept -> size_type{
		return rexy::find_first_of(*this, c, start, size);
	}
	template<class Char>
	template<class StringView>
	constexpr auto string_base<Char>::find_first_of(const StringView& str, size_type start)const noexcept(
			std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
					size_type>
	{
		const basic_string_view<value_type> tmp(str);
		return rexy::find_first_of(*this, tmp.c_str(), start, tmp.length());
	}


	template<class Char>
	constexpr auto string_base<Char>::find_last_of(value_type v, size_type start)const noexcept -> size_type{
		return rexy::find_last_of(*this, &v, start, 1);
	}
	template<class Char>
	constexpr auto string_base<Char>::find_last_of(const_pointer c, size_type start)const noexcept -> size_type{
		return rexy::find_last_of(*this, c, start, rexy::strlen(c));
	}
	template<class Char>
	constexpr auto string_base<Char>::find_last_of(const_pointer c, size_type start, size_type size)const noexcept -> size_type{
		return rexy::find_last_of(*this, c, start, size);
	}
	template<class Char>
	template<class StringView>
	constexpr auto string_base<Char>::find_last_of(const StringView& str, size_type start)const noexcept(
			std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
					size_type>
	{
		const rexy::basic_string_view<value_type> tmp(str);
		return rexy::find_last_of(*this, tmp.c_str(), start, tmp.length());
	}


	template<class Char>
	constexpr auto string_base<Char>::find_first_not_of(const_pointer str, size_type start, size_type count)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, str, start, count);
	}
	template<class Char>
	constexpr auto string_base<Char>::find_first_not_of(const_pointer str, size_type start)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, str, start, rexy::strlen(str));
	}
	template<class Char>
	constexpr auto string_base<Char>::find_first_not_of(value_type v, size_type start)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, &v, start, 1);
	}
	template<class Char>
	template<class StringView>
	constexpr auto string_base<Char>::find_first_not_of(const StringView& str, size_type start)const noexcept(
			std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
					size_type>
	{
		const basic_string_view<value_type> tmp(str);
		return rexy::find_first_not_of(*this, tmp.c_str(), start, tmp.length());
	}

	template<class Char>
	constexpr auto string_base<Char>::find_last_not_of(const_pointer str, size_type start, size_type count)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, str, start, count);
	}
	template<class Char>
	constexpr auto string_base<Char>::find_last_not_of(const_pointer str, size_type start)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, str, start, rexy::strlen(str));
	}
	template<class Char>
	constexpr auto string_base<Char>::find_last_not_of(value_type v, size_type start)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, &v, start, 1);
	}
	template<class Char>
	template<class StringView>
	constexpr auto string_base<Char>::find_last_not_of(const StringView& str, size_type start)const noexcept(
			std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
					size_type>
	{
		const basic_string_view<value_type> tmp(str);
		return rexy::find_last_not_of(*this, tmp.c_str(), start, tmp.length());
	}


	template<class Char>
	constexpr void string_base<Char>::clear(void)noexcept{
		set_length(0);
		get_pointer()[0] = 0;
	}


	template<class Char>
	constexpr auto string_base<Char>::create_view(void)const noexcept -> basic_string_view<value_type>{
		return basic_string_view<value_type>(data(), length());
	}
	template<class Char>
	constexpr auto string_base<Char>::create_view(const_iterator start, const_iterator fin)const noexcept -> basic_string_view<value_type>{
		return basic_string_view<value_type>(start, fin);
	}



	//allocate string if longer than small string capacity, copy otherwise
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	void basic_string<Char,Alloc>::_copy_construct_string(const_pointer data, size_type len, size_type cap)noexcept(
				is_nothrow_allocator_v<Alloc>)
	{
		if constexpr(string_base<Char>::uses_sso()){
			if(cap > string_base<Char>::short_string_size()){
				pointer raw = this->set_long_ptr(this->allocate(sizeof(value_type)*(cap+1)));
				if(data){
					rexy::memcpy(raw, data, sizeof(value_type)*len);
				}
				raw[len] = 0;
				this->set_long_length(len);
				this->set_long_capacity(cap);
			}else{
				pointer raw = this->set_short_ptr();
				if(data){
					rexy::memcpy(raw, data, sizeof(value_type)*len);
				}
				raw[len] = 0;
				this->set_short_length(len);
				this->set_short_capacity(cap);
			}
		}else{
			if(cap == 0){
				return;
			}
			pointer raw = this->set_long_ptr(this->allocate(sizeof(value_type)*(cap+1)));
			if(data){
				rexy::memcpy(raw, data, sizeof(value_type)*len);
			}
			raw[len] = 0;
			this->set_length(len);
			this->set_capacity(cap);
		}
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr basic_string<Char,Alloc>::basic_string(void)noexcept{}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr basic_string<Char,Alloc>::basic_string(rexy::steal<pointer> data)noexcept:
		basic_string(data.value(), data.value() ? rexy::strlen(data.value()) : 0){}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr basic_string<Char,Alloc>::basic_string(rexy::steal<pointer> data, size_type len)noexcept:
		string_base<Char>(data.value(), len, len){}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr basic_string<Char,Alloc>::basic_string(rexy::steal<pointer> data, size_type len, size_type cap)noexcept:
		string_base<Char>(data.value(), len, cap){}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(const_pointer data, size_type len, size_type cap)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		_copy_construct_string(data, len, cap);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(const_pointer data, size_type len)noexcept(
			is_nothrow_allocator_v<Alloc>):
		basic_string(data, len, len){}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(const_pointer data)noexcept(
			is_nothrow_allocator_v<Alloc>):
		basic_string(data, data ? rexy::strlen(data) : 0){}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(size_type cap)noexcept(
			is_nothrow_allocator_v<Alloc>):
		basic_string(size_type(0), cap){}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(size_type len, size_type cap)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		_copy_construct_string(nullptr, len, cap);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(const basic_string_view<Char>& sv)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		_copy_construct_string(sv.c_str(), sv.length(), sv.length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class InputIt, class Enable>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(InputIt start, InputIt fin)noexcept(
			is_nothrow_allocator_v<Alloc>):
		basic_string(nullptr, size_type(fin - start))
	{
		auto raw = this->get_pointer();
		size_type i = 0;
		for(auto it = start;it != fin;++it,++i){
			raw[i] = *it;
		}
		raw[i] = 0;
	}

	//normal copy and move ctors
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(const basic_string& b)noexcept(
			is_nothrow_allocator_v<Alloc>):
		detail::hasallocator<Alloc>(b)
	{
		_copy_construct_string(b.data(), b.length(), b.length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr basic_string<Char,Alloc>::basic_string(basic_string&& s)noexcept:
		detail::hasallocator<Alloc>(std::move(s)),
		string_base<Char>(std::move(s)){}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::basic_string(const string_base<Char>& b)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		_copy_construct_string(b.data(), b.length(), b.length());
	}


	//dtor
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>::~basic_string(void)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		if(this->islong()){
			this->deallocate(this->get_pointer(), sizeof(value_type)*(this->capacity()+1));
		}
	}


	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>& basic_string<Char,Alloc>::operator=(const basic_string& s)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		if(s.length() < this->capacity()){
			rexy::memcpy(this->get_pointer(), s.get_pointer(), sizeof(value_type)*(s.length()+1));
			this->set_length(s.length());
			return *this;
		}else{
			basic_string tmp(s);
			return (*this = std::move(tmp));
		}
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr basic_string<Char,Alloc>& basic_string<Char,Alloc>::operator=(basic_string&& s)noexcept{
		string_base<Char>::operator=(std::move(s));
		return *this;
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>& basic_string<Char,Alloc>::operator=(const string_base<Char>& s)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		return (*this = basic_string(s));
	}

	//Copy from c string
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>& basic_string<Char,Alloc>::operator=(const basic_string_view<Char>& sv)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		return _copy_string(sv.c_str(), sv.length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>& basic_string<Char,Alloc>::operator=(const_pointer c)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		return _copy_string(c, rexy::strlen(c));
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::find_first_of(const basic_string& str, size_type start)const noexcept -> size_type{
		return rexy::find_first_of(*this, str.c_str(), start, str.length());
	}
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::find_last_of(const basic_string& str, size_type start)const noexcept -> size_type{
		return rexy::find_last_of(*this, str.c_str(), start, str.length());
	}
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::find_first_not_of(const basic_string& str, size_type start)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, str.c_str(), start, str.length());
	}
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::find_last_not_of(const basic_string& str, size_type start)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, str.c_str(), start, str.length());
	}


	//Replace managed pointer. Frees existing value
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	void basic_string<Char,Alloc>::reset(pointer val)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		reset(val, val ? rexy::strlen(val) : 0);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	void basic_string<Char,Alloc>::reset(pointer val, size_type len)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		if(this->islong())
			this->deallocate(this->get_pointer(),sizeof(value_type)*(this->capacity()+1));
		this->set_long_ptr(val);
		if constexpr(string_base<Char>::uses_sso()){
			this->set_long_length(len);
			this->set_long_capacity(len);
		}else{
			this->set_length(len);
			this->set_capacity(len);
		}
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	bool basic_string<Char,Alloc>::reserve(size_type newsize)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		if(newsize < this->capacity())
			return false;
		if(!this->islong() && newsize < string_base<Char>::short_string_size())
			return false;
		return (*this = basic_string(this->get_pointer(), newsize)).valid();
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	void basic_string<Char,Alloc>::shrink_to_fit(void)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		if(this->length() == this->capacity()){
			return;
		}
		*this = basic_string(this->get_pointer(), this->length(), this->length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	void basic_string<Char,Alloc>::resize(size_type newsize, value_type v)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		const auto len = this->length();
		const auto copy_count = std::min(newsize, len);
		const auto insert_count = newsize > len ? newsize - len : 0;

		basic_string newstr(newsize);
		auto* ptr = newstr.data();

		rexy::memcpy(ptr, this->data(), copy_count * sizeof(value_type));
		ptr += copy_count;

		for(size_type i = 0;i < insert_count;++i){
			*ptr++ = v;
		}
		*ptr = 0;
		newstr.set_length(newsize);

		*this = std::move(newstr);
	}


	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, size_type insert_count, value_type v)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(pos, constant_iterator{v}, insert_count);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, value_type v)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(pos, &v, 1);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, const_pointer str)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(pos, str, rexy::strlen(str));
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, const_pointer str, size_type insert_count)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(pos, str, insert_count);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, const basic_string& other)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(pos, other.begin(), other.length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, const basic_string& other, size_type index_str, size_type count)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(pos, other.begin() + index_str, count);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(const_iterator pos, value_type v)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(pos - this->begin(), &v, 1);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(const_iterator pos, size_type count, value_type v)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return insert(pos - this->begin(), count, v);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class InputIt>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(const_iterator pos, InputIt start, InputIt last)noexcept(
			is_nothrow_allocator_v<Alloc>) ->
				std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>
	{
		return insert(pos - this->begin(), std::move(start), std::move(last));
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(const_iterator pos, std::initializer_list<value_type> list)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return insert(pos, list.begin(), list.end());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class StringView>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, const StringView& sv)noexcept(
			is_nothrow_allocator_v<Alloc>) ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
					!std::is_convertible_v<const StringView&,const_pointer>,
					basic_string&>
	{
		return insert(pos, sv.begin(), sv.end());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class StringView>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, const StringView& sv, size_type index_str, size_type count)noexcept(
			is_nothrow_allocator_v<Alloc>) ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
					!std::is_convertible_v<const StringView&,const_pointer>,
					basic_string&>
	{
		return insert(pos, sv.begin() + index_str, sv.begin() + index_str + count);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class InputIt>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::insert(size_type pos, InputIt start, InputIt last)noexcept(
			is_nothrow_allocator_v<Alloc>) ->
				std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>
	{
		size_type insert_count = 0;
		for(auto it = start;it != last;++it, ++insert_count){}
		return _insert_impl(pos, start, insert_count);
	}



	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::append(const_pointer data, size_type len)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(this->length(), data, len);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::append(const_pointer data)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		if(data){
			append(data, rexy::strlen(data));
		}
		return *this;
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::append(const basic_string& other)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		return _insert_impl(this->length(), other.data(), other.length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class InputIt>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::append(InputIt start, InputIt fin)noexcept(
			is_nothrow_allocator_v<Alloc>) ->
				std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>
	{
		return insert(this->length(), start, fin);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	void basic_string<Char,Alloc>::push_back(value_type data)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		append(&data, 1);
	}


	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr void basic_string<Char,Alloc>::pop_back(void)noexcept{
		erase(this->end() - 1);
	}



	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(size_type pos, size_type count, const basic_string& str)noexcept -> basic_string&{
		return replace(pos, count, str.create_view());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(
				const_iterator first, const_iterator last,
				const basic_string& str)noexcept -> basic_string&
	{
		return replace(first, last, str.create_view());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(
				size_type pos, size_type count,
				const basic_string& str, size_type pos2, size_type count2)noexcept -> basic_string&
	{
		return replace(pos, count, str.create_view(), pos2, count2);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class InputIt>
	constexpr auto basic_string<Char,Alloc>::replace(const_iterator first, const_iterator last, InputIt first2, InputIt last2)noexcept ->
		std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>
	{
		const size_type len = last - first;
		size_type count = 0;
		for(auto it = first2;count < len && it != last2;++count,++it);
		return _replace_impl(size_type(first - this->begin()), len, first2, count);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(
				size_type pos, size_type count,
				const_pointer cstr, size_type count2)noexcept -> basic_string&
	{
		return _replace_impl(pos, count, cstr, count2);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(
				const_iterator first, const_iterator last,
				const_pointer cstr, size_type count)noexcept -> basic_string&
	{
		return _replace_impl(size_type(first - this->begin()), size_type(last - first), cstr, count);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(size_type pos, size_type count, const_pointer cstr)noexcept -> basic_string&{
		return _replace_impl(pos, count, cstr, rexy::strlen(cstr));
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(const_iterator first, const_iterator last, const_pointer cstr)noexcept -> basic_string&{
		return _replace_impl(size_type(first - this->begin()), size_type(last - first), cstr, rexy::strlen(cstr));
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(
				size_type pos, size_type count,
				size_type count2, value_type v)noexcept -> basic_string&
	{
		return _replace_impl(pos, count, constant_iterator{v}, count2);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(
				const_iterator first, const_iterator last,
				size_type count2, value_type v)noexcept -> basic_string&
	{
		return _replace_impl(size_type(first - this->begin()), size_type(last - first), constant_iterator{v}, count2);
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::replace(
				const_iterator first, const_iterator last,
				std::initializer_list<value_type> list)noexcept -> basic_string&
	{
		return _replace_impl(size_type(first - this->begin()), size_type(last - first), list.begin(), list.size());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class StringView>
	constexpr auto basic_string<Char,Alloc>::replace(size_type pos, size_type count, const StringView& sv)noexcept ->
			std::enable_if_t<
				std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
				!std::is_convertible_v<const StringView&,const_pointer>,
				basic_string&>
	{
		return _replace_impl(pos, count, sv.begin(), sv.length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class StringView>
	constexpr auto basic_string<Char,Alloc>::replace(const_iterator first, const_iterator last, const StringView& sv)noexcept ->
			std::enable_if_t<
				std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
				!std::is_convertible_v<const StringView&,const_pointer>,
				basic_string&>
	{
		return _replace_impl(size_type(first - this->begin()), size_type(last - first), sv.begin(), sv.length());
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class StringView>
	constexpr auto basic_string<Char,Alloc>::replace(
				size_type pos, size_type count,
				const StringView& sv, size_type pos2, size_type count2)noexcept ->
			std::enable_if_t<
				std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
				!std::is_convertible_v<const StringView&,const_pointer>,
				basic_string&>
	{
		if(pos2 > sv.length()){
			pos2 = sv.length();
		}
		const auto maxlen2 = sv.length() - pos2;
		if(count2 > maxlen2){
			count2 = maxlen2;
		}

		return _replace_impl(pos, count, sv.begin() + pos2, maxlen2);
	}



	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::erase(size_type index, size_type count)noexcept -> basic_string&{
		const auto len = this->length();
		const auto rem_count = std::min(count, len - index);
		const auto end_pos = index + rem_count;
		const auto relocate_count = len - end_pos + 1; //include terminator

		auto* src = this->get_pointer() + end_pos;
		auto* dst = this->get_pointer() + index;
		for(size_type i = 0;i < relocate_count;++i){
			*dst++ = *src++;
		}
		this->set_length(len - rem_count);
		return *this;
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::erase(const_iterator pos)noexcept -> iterator{
		const auto pos_index = pos - this->begin();
		erase(pos - this->begin(), 1);
		return this->begin() + pos_index;
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	constexpr auto basic_string<Char,Alloc>::erase(const_iterator first, const_iterator last)noexcept -> iterator{
		const auto distance = last - first;
		const auto start_pos = first - this->begin();
		erase(start_pos, distance);
		return this->begin() + start_pos;
	}



	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<REXY_ALLOCATOR_CONCEPT A>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::substring(size_type start, size_type end)const noexcept(
				is_nothrow_allocator_v<A>) -> basic_string<value_type,A>
	{
		if(start > end || end > this->length())
			return {};
		const size_type newlen = end - start;
		basic_string<value_type,Alloc> tmp(newlen);
		tmp.append(this->data() + start, newlen);
		return tmp;
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::substr(size_type start, size_type end)const noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string
	{
		return substring<allocator_type>(start, end);
	}



	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::release(void)noexcept(
			is_nothrow_allocator_v<Alloc>) -> pointer
	{
		if(this->islong()){
			pointer raw = this->get_pointer();
			this->set_short_ptr();
			this->set_length(0);
			return raw;
		}
		if constexpr(string_base<Char>::uses_sso()){
			size_type len = this->length();
			pointer raw = this->get_pointer();
			pointer retval = this->allocate(sizeof(value_type)*len+1);
			rexy::memcpy(retval, raw, sizeof(value_type)*len);
			retval[len] = 0;
			raw[0] = 0;
			this->set_length(0);
			return retval;
		}else{
			return nullptr; //not possible to reach
		}
	}



	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	basic_string<Char,Alloc>& basic_string<Char,Alloc>::_copy_string(const_pointer s, size_type len)noexcept(
			is_nothrow_allocator_v<Alloc>)
	{
		if(!s || !len)
			return (*this = basic_string(rexy::steal<pointer>(nullptr), 0, 0));
		if(len <= this->length()){
			this->set_length(len);
			pointer raw = this->get_pointer();
			rexy::memcpy(raw, s, sizeof(value_type)*len);
			raw[len] = 0;
			return *this;
		}
		return (*this = basic_string(s, len));
	}

	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class InputIt>
	REXY_CPP20_CONSTEXPR
	auto basic_string<Char,Alloc>::_insert_impl(size_type pos, InputIt start, size_type insert_count)noexcept(
			is_nothrow_allocator_v<Alloc>) -> basic_string&
	{
		const size_type len = this->length();
		const size_type cap = this->capacity();
		//add one for null terminator
		const size_type after_pos_count = (len > pos ? len - pos : 0) + 1;

		if(insert_count + len <= cap){

			auto* dest_ptr = this->data() + len + insert_count;
			const auto* src_ptr = this->data() + len;
			for(size_type i = 0;i < after_pos_count;++i){
				*dest_ptr-- = *src_ptr--;
			}
			dest_ptr = this->data() + pos;
			for(size_type i = 0;i < insert_count;++i){
				*dest_ptr++ = *start++;
			}
			this->data()[len + insert_count] = 0; //null terminator
			this->set_length(len + insert_count);

			return *this;

		}else{

			basic_string newstr(rexy::max(pos + insert_count, len + insert_count));
			auto* ptr = newstr.get_pointer();

			rexy::memcpy(ptr, this->get_pointer(), sizeof(value_type) * pos);
			ptr += pos;

			for(size_type i = 0;i < insert_count;++i){
				*ptr++ = *start++;
			}

			rexy::memcpy(ptr, this->get_pointer() + pos, sizeof(value_type) * after_pos_count);
			newstr.set_length(len + insert_count);

			return (*this = std::move(newstr));

		}
	}
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc>
	template<class InputIt>
	constexpr auto basic_string<Char,Alloc>::_replace_impl(size_type pos, size_type count, InputIt src, size_type count2)noexcept -> basic_string&{
		const auto len = this->length();
		if(pos > len){
			pos = len;
		}
		const auto maxlen = len - pos;
		if(count > maxlen){
			count = maxlen;
		}

		const auto real_count = std::min(count, count2);

		auto* dest_ptr = this->get_pointer() + pos;
		for(size_type i = 0;i < real_count;++i){
			*dest_ptr++ = *src++;
		}

		return *this;
	}

	template<class Left, class Right>
	constexpr auto string_cat_expr<Left,Right>::length(void)const noexcept -> size_type{
		return this->m_l.length() + this->m_r.length();
	}
	template<class Left, class Right>
	template<REXY_ALLOCATOR_CONCEPT Alloc>
	REXY_CPP20_CONSTEXPR
	string_cat_expr<Left,Right>::operator basic_string<typename string_cat_expr<Left,Right>::value_type,Alloc>(void)noexcept(
			std::is_nothrow_constructible_v<
				basic_string<value_type,Alloc>,
				typename basic_string<value_type,Alloc>::size_type> &&
			std::is_nothrow_invocable_v<
				detail::string_appender<basic_string<value_type,Alloc>>,
				decltype(*this)>)
	{
		size_type len = length();
		basic_string<value_type,Alloc> ret(len);
		detail::string_appender<basic_string<value_type,Alloc>> append(ret);
		append(*this);
		return ret;
	}

} //namespace rexy

#endif
