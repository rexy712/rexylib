/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STRING_VIEW_TPP
#define REXY_STRING_VIEW_TPP

#include "compat/to_address.hpp"
#include "utility.hpp"
#include "string_base.hpp"
#include "algorithm.hpp" //two_way_search

#include "compat/string_base.hpp"

namespace rexy{

	template<class Char>
	constexpr basic_string_view<Char>::basic_string_view(const_pointer str, size_type len)noexcept:
		m_data(str), m_length(len){}
	template<class Char>
	constexpr basic_string_view<Char>::basic_string_view(const string_base<Char>& s)noexcept:
		m_data(s.c_str()), m_length(s.length()){}
	template<class Char>
	template<class InIter>
	constexpr basic_string_view<Char>::basic_string_view(InIter start, InIter fin)noexcept:
		basic_string_view(compat::to_address(start), fin - start){}

	template<class Char>
	constexpr basic_string_view<Char>::basic_string_view(const_pointer c)noexcept:
		basic_string_view(c, rexy::strlen(c)){}
	template<class Char>
	constexpr basic_string_view<Char>& basic_string_view<Char>::operator=(const_pointer c)noexcept{
		m_data = c;
		m_length = rexy::strlen(c);
		return *this;
	}

	template<class Char>
	constexpr auto basic_string_view<Char>::search(basic_string_view s)const noexcept -> const_iterator{
		if(s.length() > m_length){
			return cend();
		}
		return two_way_search(cbegin(), cend(), s.cbegin(), s.cend());
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::search(const_pointer c)const noexcept -> const_iterator{
		basic_string_view tmp(c);
		return search(tmp);
	}
	template<class Char>
	template<class Searcher>
	constexpr auto basic_string_view<Char>::search(basic_string_view s, const Searcher& searcher)const noexcept(
			std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_iterator, const_iterator>) -> const_iterator
	{
		if(s.length() > m_length){
			return cend();
		}
		return searcher(cbegin(), cend(), s.cbegin(), s.cend());
	}
	template<class Char>
	template<class Searcher>
	constexpr auto basic_string_view<Char>::search(const_pointer c, const Searcher& searcher)const noexcept(
				std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_pointer, const_pointer>) -> const_iterator
	{
		basic_string_view tmp(c);
		return search(tmp, searcher);
	}

	template<class Char>
	constexpr bool basic_string_view<Char>::starts_with(basic_string_view sv)const noexcept{
		if(sv.length() > length()){
			return false;
		}
		auto it = two_way_search(begin(), begin() + sv.length(), sv.cbegin(), sv.cend());
		if(it == begin()){
			return true;
		}
		return false;
	}
	template<class Char>
	constexpr bool basic_string_view<Char>::starts_with(value_type v)const noexcept{
		return front() == v;
	}
	template<class Char>
	constexpr bool basic_string_view<Char>::starts_with(const_pointer s)const noexcept{
		return starts_with(basic_string_view(s));
	}

	template<class Char>
	constexpr bool basic_string_view<Char>::ends_with(basic_string_view sv)const noexcept{
		if(sv.length() > length()){
			return false;
		}
		const auto start = end() - sv.length();
		auto it = two_way_search(start, end(), sv.cbegin(), sv.cend());
		if(it == start){
			return true;
		}
		return false;
	}
	template<class Char>
	constexpr bool basic_string_view<Char>::ends_with(value_type v)const noexcept{
		return back() == v;
	}
	template<class Char>
	constexpr bool basic_string_view<Char>::ends_with(const_pointer s)const noexcept{
		return ends_with(basic_string_view(s));
	}

	template<class Char>
	constexpr bool basic_string_view<Char>::contains(basic_string_view sv)const noexcept{
		const auto it = two_way_search(cbegin(), cend(), sv.cbegin(), sv.cend());
		if(it != cend()){
			return true;
		}
		return false;
	}
	template<class Char>
	constexpr bool basic_string_view<Char>::contains(value_type v)const noexcept{
		for(size_type i = 0;i < length();++i){
			if(m_data[i] == v){
				return true;
			}
		}
		return false;
	}
	template<class Char>
	constexpr bool basic_string_view<Char>::contains(const_pointer str)const noexcept{
		return contains(basic_string_view(str));
	}


	template<class Char>
	constexpr basic_string_view<Char> basic_string_view<Char>::substr(size_type pos, size_type count)const noexcept{
		const size_type real_count = rexy::min(count, length() - pos);
		return basic_string_view{m_data + pos, real_count};
	}

	template<class Char>
	constexpr void basic_string_view<Char>::remove_prefix(size_type i) noexcept{
		if(i > m_length){
			m_data = end();
			m_length = 0;
		}else{
			m_data = begin() + i;
			m_length -= i;
		}
	}
	template<class Char>
	constexpr void basic_string_view<Char>::remove_suffix(size_type i) noexcept{
		if(i > m_length){
			m_length = 0;
		}else{
			m_length -= i;
		}
	}

	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_of(basic_string_view str, size_type pos)const noexcept -> size_type{
		return rexy::find_first_of(*this, str.c_str(), pos, str.length());
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_of(value_type v, size_type start)const noexcept -> size_type{
		return rexy::find_first_of(*this, &v, start, 1);
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_of(const_pointer c, size_type start)const noexcept -> size_type{
		return rexy::find_first_of(*this, c, start, rexy::strlen(c));
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_of(const_pointer c, size_type start, size_type size)const noexcept -> size_type{
		return rexy::find_first_of(*this, c, start, size);
	}

	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_not_of(basic_string_view str, size_type pos)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, str.c_str(), pos, str.length());
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_not_of(value_type v, size_type start)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, &v, start, 1);
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_not_of(const_pointer c, size_type start)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, c, start, rexy::strlen(c));
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_first_not_of(const_pointer c, size_type start, size_type size)const noexcept -> size_type{
		return rexy::find_first_not_of(*this, c, start, size);
	}

	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_of(basic_string_view str, size_type pos)const noexcept -> size_type{
		return rexy::find_last_of(*this, str.c_str(), pos, str.length());
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_of(value_type v, size_type start)const noexcept -> size_type{
		return rexy::find_last_of(*this, &v, start, 1);
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_of(const_pointer c, size_type start)const noexcept -> size_type{
		return rexy::find_last_of(*this, c, start, rexy::strlen(c));
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_of(const_pointer c, size_type start, size_type size)const noexcept -> size_type{
		return rexy::find_last_of(*this, c, start, size);
	}

	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_not_of(basic_string_view str, size_type pos)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, str.c_str(), pos, str.length());
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_not_of(value_type v, size_type start)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, &v, start, 1);
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_not_of(const_pointer c, size_type start)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, c, start, rexy::strlen(c));
	}
	template<class Char>
	constexpr auto basic_string_view<Char>::find_last_not_of(const_pointer c, size_type start, size_type size)const noexcept -> size_type{
		return rexy::find_last_not_of(*this, c, start, size);
	}

}

#endif
