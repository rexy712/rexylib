/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_ALGORITHM_TPP
#define REXY_ALGORITHM_TPP

#include <cstddef> //size_t
#include <limits> //numeric_limits

namespace rexy{

	//Requires Iterators to be LegacyRandomAccessIterators
	//right is one past the end of the list
	template<class Iter, class Compare>
	constexpr void quicksort(Iter left, Iter right, const Compare& cmp)
		noexcept(noexcept(detail::qs_partition(left, right, cmp)))
	{
		while(left < right){
			auto real_right = right-1;
			auto pivot = detail::qs_partition(left, real_right, cmp);
			quicksort(left, pivot, cmp);
			left = ++pivot;
		}
	}

	//Requires Iterators to be LegacyRandomAccessIterators
  template<class HIter, class NIter>
  constexpr HIter two_way_search(HIter hstart, HIter hend, NIter nstart, NIter nend){
		std::size_t j = 0;
		std::size_t i = 0;
		std::size_t nlen = nend - nstart;
		std::size_t hlen = hend - hstart;
		auto [suffix, period] = detail::critical_factorization(nstart, nend);

		if(detail::iter_compare(nstart, nstart + period, suffix)){
			std::size_t memory = std::numeric_limits<std::size_t>::max();
			while(j <= hlen - nlen){
				i = max(suffix, memory) + 1;
				//right side
				while(i < nlen && nstart[i] == hstart[i + j]){
					++i;
				}
				if(i >= nlen){
					i = suffix;
					//left side
					while(i > memory && nstart[i] == hstart[i + j]){
						--i;
					}
					if(i <= memory){
						return hstart + j;
					}
					j += period;
					memory = nlen - period - 1;
				}else{
					j += (i - suffix);
					memory = std::numeric_limits<std::size_t>::max();
				}
			}
		}else{
			period = max(suffix + 1, nlen - suffix - 1) + 1;
			j = 0;
			while(j <= hlen - nlen){
				i = suffix + 1;
				//right side
				while(i < nlen && nstart[i] == hstart[i + j]){
					++i;
				}
				if(i >= nlen){
					i = suffix;
					//left side
					while(i != std::numeric_limits<std::size_t>::max() && nstart[i] == hstart[i + j]){
						--i;
					}
					if(i == std::numeric_limits<std::size_t>::max()){
						return hstart + j;
					}
					j += period;
				}else{
					j += (i - suffix);
				}
			}
		}
		return hend;
	}

	template<class HIter, class NIter>
	constexpr HIter two_way_searcher::operator()(HIter hstart, HIter hend, NIter nstart, NIter nend)const{
    return two_way_search(hstart, hend, nstart, nend);
	}

}

#endif
