/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STEAL_HPP
#define REXY_STEAL_HPP

#include <utility> //forward
#include <type_traits> //is_nothrow_constructible

#include "rexy.hpp"

namespace rexy{

	template<class T>
	class steal
	{
	private:
		T m_val;

	public:
		template<class U>
		constexpr explicit steal(U&& u)
			noexcept(std::is_nothrow_constructible<T,U&&>::value):
			m_val(std::forward<U>(u)){}

		steal(const steal&) = delete;
		steal(steal&&) = delete;
		steal& operator=(const steal&) = delete;
		steal& operator=(steal&&) = delete;

		constexpr T&& value(void)noexcept{
			return std::forward<T>(m_val);
		}
		constexpr const T& value(void)const noexcept{
			return m_val;
		}
	};

	template<class T>
	steal(const T&) -> steal<const T&>;
	template<class T>
	steal(T&&) -> steal<T&&>;
	template<class T>
	steal(T) -> steal<T>;
	template<class T>
	steal(T*) -> steal<T*>;
}

#endif
