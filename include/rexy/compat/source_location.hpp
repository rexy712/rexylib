/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_UTIL_SOURCE_LOCATION_HPP
#define REXY_UTIL_SOURCE_LOCATION_HPP

#include <version> //feature test macro

#ifndef __cpp_lib_source_location

	#include "cpp17/source_location.hpp"

	namespace rexy::compat{

		using source_location = cpp17::source_location;

	}

#else //__cpp_lib_source_location

	#include "cpp20/source_location.hpp"

	namespace rexy::compat{

		using source_location = cpp20::source_location;

	}

#endif //__cpp_lib_source_location


#endif

