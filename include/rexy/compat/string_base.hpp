/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_STRING_BASE_HPP
#define REXY_COMPAT_STRING_BASE_HPP

#include <cstddef> //size_t

namespace rexy{
	static constexpr std::size_t npos = std::size_t(-1);
}

#ifdef __cpp_concepts
	#include "cpp20/string_base.hpp"
#else
	#include "cpp17/string_base.hpp"
#endif

#endif
