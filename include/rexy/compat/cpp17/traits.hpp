/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_CPP17_TRAITS_HPP
#define REXY_COMPAT_CPP17_TRAITS_HPP

namespace rexy{

	template<class T>
	struct remove_volatile{
		using type = T;
	};
	template<class T>
	struct remove_volatile<volatile T>{
		using type = T;
	};
	template<class T>
	using remove_volatile_t = typename remove_volatile<T>::type;
	template<class T>
	struct remove_const{
		using type = T;
	};
	template<class T>
	struct remove_const<const T>{
		using type = T;
	};
	template<class T>
	using remove_const_t = typename remove_const<T>::type;
	template<class T>
	struct remove_cv{
		using type = remove_volatile_t<remove_const_t<T>>;
	};
	template<class T>
	using remove_cv_t = typename remove_cv<T>::type;
	template<class T>
	struct remove_reference{
		using type = T;
	};
	template<class T>
	struct remove_reference<T&>{
		using type = T;
	};
	template<class T>
	struct remove_reference<T&&>{
		using type = T;
	};
	template<class T>
	using remove_reference_t = typename remove_reference<T>::type;
	template<class T>
	struct remove_cvref{
		using type = remove_cv_t<remove_reference_t<T>>;
	};
	template<class T>
	using remove_cvref_t = typename remove_cvref<T>::type;

}

#endif
