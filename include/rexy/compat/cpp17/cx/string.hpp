/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_CPP17_CX_STRING_HPP
#define REXY_COMPAT_CPP17_CX_STRING_HPP

#include "../../../string_base.hpp" //string_cat_expr
#include "../../../string_view.hpp" //string_view
#include <utility> //forward
#include <type_traits> //enable_if

namespace rexy::cx{

	template<class Str1, class Str2, std::enable_if_t<is_cx_string<Str1,Str2>::value,int> = 0>
	constexpr auto operator+(Str1&& l, Str2&& r)noexcept{
		return string_cat_expr(std::forward<Str1>(l), std::forward<Str2>(r));
	}
	template<class Str1, std::enable_if_t<is_cx_string<Str1>::value,int> = 0>
	constexpr auto operator+(Str1&& l, const char* r)noexcept{
		return string_cat_expr(std::forward<Str1>(l), rexy::basic_string_view<char>(r));
	}
	template<class Str1, std::enable_if_t<is_cx_string<Str1>::value,int> = 0>
	constexpr auto operator+(const char* l, Str1&& r)noexcept{
		return string_cat_expr(rexy::basic_string_view<char>(l), std::forward<Str1>(r));
	}

}

#endif
