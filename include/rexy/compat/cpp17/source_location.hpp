/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_17_SOURCE_LOCATION_HPP
#define REXY_COMPAT_17_SOURCE_LOCATION_HPP

#include <cstdint> //uint_least32_t

//clang bug: https://bugs.llvm.org/show_bug.cgi?id=48886
#if defined(__cpp_consteval) && !defined(__clang__)
	#define CONSTEVAL consteval
#else
	#define CONSTEVAL constexpr
#endif

namespace rexy::compat::cpp17{

	class source_location
	{
	private:
		const char*    m_file;
		const char*    m_func;
		uint_least32_t m_line;
		uint_least32_t m_column;

	public:
		constexpr source_location(void)noexcept;
		constexpr source_location(const source_location&);
		constexpr source_location(source_location&&)noexcept;

		constexpr uint_least32_t line(void)const noexcept;
		constexpr uint_least32_t column(void)const noexcept;
		constexpr const char* file_name(void)const noexcept;
		constexpr const char* function_name(void)const noexcept;

	public:
	#if (defined(__clang__) && (__clang_major__ >= 9)) || (defined(_MSC_VER) && (_MSC_VER >= 1926))
		static CONSTEVAL source_location current(uint_least32_t line = __builtin_LINE(),
		                                         uint_least32_t col  = __builtin_COLUMN(),
		                                         const char* file  = __builtin_FILE(),
		                                         const char* func  = __builtin_FUNCTION())noexcept;
	#elif defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8))
		static CONSTEVAL source_location current(uint_least32_t line = __builtin_LINE(),
		                                         uint_least32_t col  = 0,
		                                         const char* file  = __builtin_FILE(),
		                                         const char* func  = __builtin_FUNCTION())noexcept;
	#else
		static CONSTEVAL source_location current(uint_least32_t line = 0,
		                                         uint_least32_t col  = 0,
		                                         const char* file  = "unknown",
		                                         const char* func  = "unknown")noexcept;
	#endif

	private:
		constexpr source_location(uint_least32_t line, uint_least32_t col, const char* file, const char* func)noexcept;
	};

	constexpr source_location::source_location(void)noexcept:
		source_location(0, 0, "unknown", "unknown"){}
	constexpr source_location::source_location(const source_location& s):
		source_location(s.m_line, s.m_column, s.m_file, s.m_func){}
	constexpr source_location::source_location(source_location&& s)noexcept:
		source_location(s.m_line, s.m_column, s.m_file, s.m_func){}
	constexpr source_location::source_location(uint_least32_t line, uint_least32_t col, const char* file, const char* func)noexcept:
		m_file(file),
		m_func(func),
		m_line(line),
		m_column(col){}

	constexpr uint_least32_t source_location::line(void)const noexcept{
		return m_line;
	}
	constexpr uint_least32_t source_location::column(void)const noexcept{
		return m_column;
	}
	constexpr const char* source_location::file_name(void)const noexcept{
		return m_file;
	}
	constexpr const char* source_location::function_name(void)const noexcept{
		return m_func;
	}

	CONSTEVAL source_location source_location::current(uint_least32_t line, uint_least32_t col, const char* file, const char* func)noexcept{
		return source_location(line, col, file, func);
	}


#undef CONSTEVAL

}

#endif
