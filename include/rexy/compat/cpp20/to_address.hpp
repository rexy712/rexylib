/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_20_TO_ADDRESS_HPP
#define REXY_COMPAT_20_TO_ADDRESS_HPP

#include <memory> //to_address

namespace rexy::compat::cpp20{

	template<class T>
	constexpr T* to_address(T* p)noexcept{
		return std::to_address(p);
	}
	template<class Ptr>
	constexpr auto to_address(const Ptr& p)noexcept{
		return std::to_address(p);
	}

}

#endif
