/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_CPP17_TRAITS_HPP
#define REXY_COMPAT_CPP17_TRAITS_HPP

#include <type_traits> //remove_cvref

namespace rexy{

	template<class T>
	struct remove_cvref : public std::remove_cvref<T>{};
	template<class T>
	using remove_cvref_t = typename remove_cvref<T>::type;

}

#endif
