/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_if_consteval

#if defined(__cpp_if_consteval)
	#define REXY_if_consteval if consteval
	#define REXY_if_not_consteval if not consteval
#elif defined(__cpp_lib_is_constant_evaluated)
	#include <type_traits> //is_constant_evaluated
	#define REXY_if_consteval if(std::is_constant_evaluated())
	#define REXY_if_not_consteval if(!std::is_constant_evaluated())
#endif //__cpp_if_consteval

#endif
