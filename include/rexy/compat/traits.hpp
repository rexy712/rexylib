/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_TRAITS_HPP
#define REXY_COMPAT_TRAITS_HPP

#ifdef REXY_STANDARD_CPP20
	#include "cpp20/traits.hpp"
#else // REXY_STANDARD_CPP26
	#include "cpp17/traits.hpp"
#endif // REXY_STANDARD_CPP26


#endif
