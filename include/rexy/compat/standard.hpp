/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_COMPAT_CPP20_HPP
#define REXY_COMPAT_CPP20_HPP

	#define REXY_REQUIRES_CPP20 static_assert(__cplusplus >= 202002L, "C++20 is required to use this file.")

	#if __cplusplus >= 202002L
		#define REXY_CPP20_CONSTEXPR constexpr
		#define REXY_CPP20_CONSTEVAL consteval
	#else //__cpp_consteval
		#define REXY_CPP20_CONSTEXPR
		#define REXY_CPP20_CONSTEVAL constexpr
	#endif //__cpp_consteval

	#if __cplusplus >= 202300L
		#define REXY_STANDARD_CPP23
	#endif
	#if __cplusplus >= 202002L
		#define REXY_STANDARD_CPP20
	#endif
	#if __cplusplus >= 201703L
		#define REXY_STANDARD_CPP17
	#endif
	#if __cplusplus < 201703L
		#error "Requires minimum C++17 standard"
	#endif //__cplusplus

#endif
