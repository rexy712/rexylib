/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STRING_VIEW_HPP
#define REXY_STRING_VIEW_HPP

#include <cstddef> //std::size_t, ptrdiff_t
#include <iterator> //reverse_iterator

#include "compat/standard.hpp"
#include "rexy.hpp"

namespace rexy{

	template<class Char>
	class string_base;

	template<class Char>
	class basic_string_view
	{
	public:
		using value_type = Char;
		using size_type = std::size_t;
		using difference_type = ptrdiff_t;
		using pointer = value_type*;
		using const_pointer = const value_type*;
		using reference = value_type&;
		using const_reference = const value_type&;
		using iterator = const_pointer;
		using const_iterator = const_pointer;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		static constexpr size_type npos = size_type(-1);

	private:
		const_pointer m_data = nullptr;
		size_type m_length = 0;

	public:
		constexpr basic_string_view(void)noexcept = default;
		constexpr basic_string_view(const_pointer str, size_type len)noexcept;
		constexpr basic_string_view(const_pointer c)noexcept;
		constexpr basic_string_view(const basic_string_view& s)noexcept = default;
		constexpr basic_string_view(const string_base<Char>& s)noexcept;
		constexpr basic_string_view(basic_string_view&& s)noexcept = default;
		template<class InIter>
		constexpr basic_string_view(InIter start, InIter fin)noexcept;
		REXY_CPP20_CONSTEXPR ~basic_string_view(void)noexcept = default;

		constexpr basic_string_view& operator=(const_pointer c)noexcept;
		constexpr basic_string_view& operator=(const basic_string_view& s)noexcept = default;
		constexpr basic_string_view& operator=(basic_string_view&&)noexcept = default;

		//Length of string not including null terminator
		constexpr size_type length(void)const noexcept{return m_length;}
		constexpr size_type size(void)const noexcept{return m_length;}
		//direct access to managed pointer
		constexpr const_pointer c_str(void)const noexcept{return m_data;}
		constexpr const_pointer data(void)const noexcept{return m_data;}
		//true if m_data is not empty
		constexpr bool valid(void)const noexcept{return m_length > 0;}
		constexpr bool empty(void)const noexcept{return m_length == 0;}

		constexpr const_reference operator[](size_type i)const noexcept{return m_data[i];}
		constexpr const_reference at(size_type i)const noexcept{return m_data[i];}
		constexpr const_reference front(void)const noexcept{return m_data[0];}
		constexpr const_reference back(void)const noexcept{return m_data[m_length-1];}
		constexpr const_iterator it_at(size_type i)const noexcept{return m_data + i;}

		constexpr const_iterator search(basic_string_view s)const noexcept;
		constexpr const_iterator search(const_pointer c)const noexcept;
		template<class Searcher>
		constexpr const_iterator search(basic_string_view s, const Searcher& searcher)const noexcept(
				std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_iterator, const_iterator>);
		template<class Searcher>
		constexpr const_iterator search(const_pointer c, const Searcher& searcher)const noexcept(
				std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_pointer, const_pointer>);

		constexpr bool starts_with(basic_string_view sv)const noexcept;
		constexpr bool starts_with(value_type v)const noexcept;
		constexpr bool starts_with(const_pointer str)const noexcept;

		constexpr bool ends_with(basic_string_view sv)const noexcept;
		constexpr bool ends_with(value_type v)const noexcept;
		constexpr bool ends_with(const_pointer str)const noexcept;

		constexpr bool contains(basic_string_view sv)const noexcept;
		constexpr bool contains(value_type sv)const noexcept;
		constexpr bool contains(const_pointer str)const noexcept;

		constexpr bool compare(const basic_string_view& s)const noexcept{return *this == s;}
		constexpr bool compare(const_pointer c)const noexcept{return *this == c;}

		constexpr const_iterator begin(void)const noexcept{return m_data;}
		constexpr const_iterator end(void)const noexcept{return m_data+m_length;}
		constexpr const_iterator cbegin(void)const noexcept{return begin();}
		constexpr const_iterator cend(void)const noexcept{return end();}

		constexpr const_reverse_iterator rbegin(void)const noexcept{return const_reverse_iterator(m_data+m_length);}
		constexpr const_reverse_iterator rend(void)const noexcept{return const_reverse_iterator(m_data);}
		constexpr const_reverse_iterator crbegin(void)const noexcept{return rbegin();}
		constexpr const_reverse_iterator crend(void)const noexcept{return rend();}

		constexpr void remove_prefix(size_type i)noexcept;
		constexpr void remove_suffix(size_type i)noexcept;

		constexpr basic_string_view substr(size_type pos, size_type count = npos)const noexcept;

		constexpr size_type find_first_of(basic_string_view str, size_type pos = 0)const noexcept;
		constexpr size_type find_first_of(value_type v, size_type start = 0)const noexcept;
		constexpr size_type find_first_of(const_pointer c, size_type pos = 0)const noexcept;
		constexpr size_type find_first_of(const_pointer c, size_type pos, size_type size)const noexcept;

		constexpr size_type find_first_not_of(basic_string_view str, size_type pos = 0)const noexcept;
		constexpr size_type find_first_not_of(value_type v, size_type start = 0)const noexcept;
		constexpr size_type find_first_not_of(const_pointer c, size_type pos = 0)const noexcept;
		constexpr size_type find_first_not_of(const_pointer c, size_type pos, size_type size)const noexcept;

		constexpr size_type find_last_of(basic_string_view str, size_type pos = 0)const noexcept;
		constexpr size_type find_last_of(value_type v, size_type start = 0)const noexcept;
		constexpr size_type find_last_of(const_pointer c, size_type pos = 0)const noexcept;
		constexpr size_type find_last_of(const_pointer c, size_type pos, size_type size)const noexcept;

		constexpr size_type find_last_not_of(basic_string_view str, size_type pos = 0)const noexcept;
		constexpr size_type find_last_not_of(value_type v, size_type start = 0)const noexcept;
		constexpr size_type find_last_not_of(const_pointer c, size_type pos = 0)const noexcept;
		constexpr size_type find_last_not_of(const_pointer c, size_type pos, size_type size)const noexcept;

	};

	template<class T>
	basic_string_view(const T*) -> basic_string_view<T>;
	template<class T>
	basic_string_view(const T*, std::size_t) -> basic_string_view<T>;

	using string_view = basic_string_view<char>;
	using wstring_view = basic_string_view<wchar_t>;

#ifndef LIBREXY_HEADER_ONLY
	extern template class basic_string_view<char>;
	extern template class basic_string_view<wchar_t>;
	extern template class basic_string_view<char16_t>;
	extern template class basic_string_view<char32_t>;
#endif

	inline namespace str_literals{

		constexpr inline rexy::basic_string_view<char> operator"" _sv(const char* str, std::size_t len)noexcept{
			return rexy::basic_string_view(str, len);
		}
		constexpr inline rexy::basic_string_view<wchar_t> operator"" _sv(const wchar_t* str, std::size_t len)noexcept{
			return rexy::basic_string_view(str, len);
		}
		constexpr inline rexy::basic_string_view<char16_t> operator"" _sv(const char16_t* str, std::size_t len)noexcept{
			return rexy::basic_string_view(str, len);
		}
		constexpr inline rexy::basic_string_view<char32_t> operator"" _sv(const char32_t* str, std::size_t len)noexcept{
			return rexy::basic_string_view(str, len);
		}

	}

}

namespace{
	template<class Char>
	std::ostream& operator<<(std::ostream& os, const rexy::basic_string_view<Char>& str){
		return os << str.c_str();
	}
}

#include "string_view.tpp"


#endif
