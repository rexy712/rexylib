/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_FILERD_HPP
#define REXY_FILERD_HPP

#ifndef LIBREXY_HEADER_ONLY

#include <cstdio> //FILE
#include <cstddef> //size_t
#include <type_traits> //is_nothrow_constructible

#include "string.hpp"
#include "steal.hpp"
#include "utility.hpp"
#include "rexy.hpp"
#include "buffer.hpp"
#include "string_view.hpp"

namespace rexy{

	//RAII wrapper for FILE*
	class filerd
	{
	private:
		std::FILE* m_fp = nullptr;
		bool m_finished = false;

	public:
		constexpr filerd(void)noexcept = default;
		filerd(const char* f, const char* mode = "r")noexcept;
		filerd(const filerd&) = delete;
		constexpr filerd(filerd&& f)noexcept:
			m_fp(exchange(f.m_fp, nullptr)){}
		~filerd(void)noexcept;
		filerd& operator=(const filerd&) = delete;
		constexpr filerd& operator=(filerd&& f)noexcept{
			rexy::swap(m_fp, f.m_fp);
			return *this;
		}

		void reset(std::FILE* fp = nullptr)noexcept;
		std::FILE* release(void)noexcept;
		std::size_t length(void)noexcept;
		std::size_t position(void)const noexcept;
		void rewind(std::size_t pos = 0)noexcept;

		operator std::FILE*(void)noexcept;
		operator const std::FILE*(void)const noexcept;
		std::FILE* get(void)noexcept;
		const std::FILE* get(void)const noexcept;
		operator bool(void)const noexcept;

		bool eof(void)const;

		std::size_t read(char* dest, std::size_t bytes)noexcept;
		rexy::string read(std::size_t bytes)noexcept;
		rexy::string readln(std::size_t max = 0)noexcept;
		rexy::buffer<char> read_bin(std::size_t bytes)noexcept(std::is_nothrow_constructible<rexy::buffer<char>, char*, std::size_t>::value);

		std::size_t write(const char* c, std::size_t bytes)noexcept;
		std::size_t write(rexy::string_view s)noexcept;
	};

}

#else //LIBREXY_HEADER_ONLY

#error "rexy::filerd is not available when built with header only support"

#endif //LIBREXY_HEADER_ONLY

#endif
