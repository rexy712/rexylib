/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_DEFAULT_ALLOCATOR_HPP
#define REXY_DEFAULT_ALLOCATOR_HPP

#include "compat/allocator.hpp"

#include <type_traits> //declval

namespace rexy{

	template<class T, class U>
	constexpr bool operator==(const allocator<T>&, const allocator<U>&){
		return true;
	}
	template<class T, class U>
	constexpr bool operator!=(const allocator<T>&, const allocator<U>&){
		return false;
	}

	template<REXY_ALLOCATOR_CONCEPT T>
	struct is_nothrow_allocator{
		static constexpr bool value = noexcept(std::declval<T>().allocate(0)) && noexcept(std::declval<T>().deallocate(nullptr, 0));
	};
	template<REXY_ALLOCATOR_CONCEPT T>
	static constexpr bool is_nothrow_allocator_v = is_nothrow_allocator<T>::value;

}

#endif
