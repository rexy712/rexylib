/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_THREADPOOL_HPP
#define REXY_THREADPOOL_HPP

#include <vector>
#include <queue>
#include <condition_variable>
#include <mutex> //unique_lock, scoped_lock
#include <shared_mutex> //shared_mutex, shared_lock
#include <atomic> //atomic_bool
#include <memory> //shared_ptr
#include <future> //future, packaged_task
#include <functional> //function, bind
#include <utility> //move, forward
#include <type_traits> //invoke_result_t

#include "rexy.hpp"

namespace rexy{

	class threadpool
	{
	private:
		using mutex_t = std::shared_mutex;
		using write_lock_t = std::unique_lock<mutex_t>;
		using read_lock_t = std::shared_lock<mutex_t>;

	private:
		mutable read_lock_t m_ctor_lock;
		mutable std::condition_variable_any m_qcv;
		mutable mutex_t m_qlk;

		std::vector<std::thread> m_workers;
		std::queue<std::function<void()>> m_jobs;
		std::atomic_bool m_valid = true;

	public:
		explicit threadpool(int numthreads = std::thread::hardware_concurrency());
		threadpool(const threadpool&);
		threadpool(threadpool&&);
		~threadpool(void);

		threadpool& operator=(const threadpool&) = delete;
		threadpool& operator=(threadpool&&) = delete;

		void invalidate(void);

		template<class Func, class... Args>
		auto add_job(Func&& f, Args&&... args) -> std::future<std::invoke_result_t<Func,Args...>>;

	private:
		void worker_loop(void);
	};

	template<class Func, class... Args>
	auto threadpool::add_job(Func&& f, Args&&... args) -> std::future<std::invoke_result_t<Func,Args...>>{
		using return_t = decltype(std::forward<Func>(f)(std::forward<Args>(args)...));
		using task_t = std::packaged_task<return_t(void)>;

		//shared pointer to a packaged task which takes no arguments in operator()
		std::shared_ptr<task_t> task_ptr = std::make_shared<task_t>(std::bind(std::forward<Func>(f), std::forward<Args>(args)...));

		{
			write_lock_t lk(m_qlk);
			//make a copy of the shared pointer by capturing by-value
			m_jobs.emplace([task_ptr]{(*task_ptr)();});
		}

		//wakeup a worker to run the job
		m_qcv.notify_one();
		return task_ptr->get_future();
	}

}

#endif
