/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_META_HPP
#define REXY_META_HPP

#include "rexy.hpp"

namespace rexy{

	template<int... Is>
	struct sequence_tuple{};
	template<int I, int... Is>
	struct sequence_gen : public sequence_gen<I-1, Is..., sizeof...(Is)>{};
	template<int... Is>
	struct sequence_gen<0,Is...>{
		using type = sequence_tuple<Is...>;
	};
	template<int... Is>
	using sequence_gen_t = typename sequence_gen<Is...>::type;

}

#endif
