/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STORAGE_FOR_HPP
#define REXY_STORAGE_FOR_HPP

#include <utility> //forward, move
#include <type_traits> //is_nothrow_constructible, etc

#include "compat/standard.hpp"

namespace rexy{

	//A wrapper around raw storage.
	//Allows default constructing objects with members which cannot be default constructed

	template<class T>
	class storage_for
	{
	public:
		using value_type = T;
		using pointer = T*;
		using const_pointer = const T*;
		using reference = T&;
		using const_reference = const T&;
		using rvalue_reference = T&&;

		static constexpr auto align = alignof(value_type);
		static constexpr auto size = sizeof(value_type);

	private:
#if __cplusplus >= 202002L && __has_cpp_attribute(no_unique_address) && __cpp_constexpr >= 202002L

		union storage_{
			[[no_unique_address]]
			struct empty_{}empty = {};
			T data;
		}m_storage;

#else //__cplusplus

		alignas(align) unsigned char m_storage[size];

#endif //__cplusplus
		unsigned char m_dirty:1 = 0;

	public:
		constexpr storage_for(void) = default;
		template<class... Args>
		constexpr storage_for(Args&&... args)noexcept(std::is_nothrow_constructible<value_type,Args...>::value);
		constexpr storage_for(const_reference t)noexcept(std::is_nothrow_copy_constructible<value_type>::value);
		constexpr storage_for(rvalue_reference t)noexcept(std::is_nothrow_move_constructible<value_type>::value);
		constexpr storage_for(const storage_for& s)noexcept(std::is_nothrow_copy_constructible<value_type>::value);
		constexpr storage_for(storage_for&& s)noexcept(std::is_nothrow_move_constructible<value_type>::value);
		REXY_CPP20_CONSTEXPR ~storage_for(void)noexcept(std::is_nothrow_destructible<value_type>::value);

		constexpr storage_for& operator=(const storage_for& s)noexcept(std::is_nothrow_copy_assignable<value_type>::value);
		constexpr storage_for& operator=(storage_for&& s)noexcept(std::is_nothrow_move_assignable<value_type>::value);
		constexpr storage_for& operator=(const_reference t)noexcept(std::is_nothrow_copy_assignable<value_type>::value);
		constexpr storage_for& operator=(rvalue_reference t)noexcept(std::is_nothrow_move_assignable<value_type>::value);
		constexpr void destroy(void)noexcept(std::is_nothrow_destructible<value_type>::value);

		constexpr operator reference(void)noexcept;
		constexpr operator const_reference(void)const noexcept;
		constexpr operator rvalue_reference(void)&& noexcept;

		constexpr reference get(void)noexcept;
		constexpr const_reference get(void)const noexcept;

		constexpr pointer operator->(void)noexcept;
		constexpr const_pointer operator->(void)const noexcept;
		constexpr reference operator*(void)noexcept;
		constexpr const_reference operator*(void)const noexcept;

		constexpr bool valid(void)const;
	};

}

#include "storage_for.tpp"

#endif
