/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STORAGE_FOR_TPP
#define REXY_STORAGE_FOR_TPP

#include <utility> //forward, move

#if __cplusplus >= 202002L && __has_cpp_attribute(no_unique_address) && __cpp_constexpr >= 202002L

#include <memory> //construct_at, destroy_at

namespace rexy{

	template<class T>
	template<class... Args>
	constexpr storage_for<T>::storage_for(Args&&... args)noexcept(std::is_nothrow_constructible<value_type,Args...>::value){
		std::construct_at(&m_storage.data, std::forward<Args>(args)...);
		m_dirty = 1;
	}
	template<class T>
	constexpr storage_for<T>::storage_for(const_reference t)noexcept(std::is_nothrow_copy_constructible<value_type>::value){
		std::construct_at(&m_storage.data, t);
		m_dirty = 1;
	}
	template<class T>
	constexpr storage_for<T>::storage_for(rvalue_reference t)noexcept(std::is_nothrow_move_constructible<value_type>::value){
		std::construct_at(&m_storage.data, std::move(t));
		m_dirty = 1;
	}
	template<class T>
	constexpr storage_for<T>::storage_for(const storage_for& s)noexcept(std::is_nothrow_copy_constructible<value_type>::value){
		if(s.m_dirty){
			std::construct_at(&m_storage.data, s.m_storage.data);
			m_dirty = 1;
		}
	}
	template<class T>
	constexpr storage_for<T>::storage_for(storage_for&& s)noexcept(std::is_nothrow_move_constructible<value_type>::value){
		if(s.m_dirty){
			std::construct_at(&m_storage.data, std::move(s.m_storage.data));
			m_dirty = 1;
		}
	}
	template<class T>
	REXY_CPP20_CONSTEXPR storage_for<T>::~storage_for(void)noexcept(std::is_nothrow_destructible<value_type>::value){
		if(m_dirty){
			destroy();
		}
	}

	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(const storage_for& s)noexcept(std::is_nothrow_copy_assignable<value_type>::value){
		return ((*this) = storage_for(s));
	}
	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(storage_for&& s)noexcept(std::is_nothrow_move_assignable<value_type>::value){
		if(s.m_dirty){
			return ((*this) = std::move(s.m_storage.data));
		}else{
			if(m_dirty){
				destroy();
			}
		}
		return *this;
	}
	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(const_reference t)noexcept(std::is_nothrow_copy_assignable<value_type>::value){
		if(m_dirty){
			m_storage.data = t;
		}else{
			std::construct_at(&m_storage.data, t);
		}
		m_dirty = 1;
		return *this;
	}
	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(rvalue_reference t)noexcept(std::is_nothrow_move_assignable<value_type>::value){
		if(m_dirty){
			m_storage.data = std::move(t);
		}else{
			std::construct_at(&m_storage.data, std::move(t));
		}
		m_dirty = 1;
		return *this;
	}
	template<class T>
	constexpr void storage_for<T>::destroy(void)noexcept(std::is_nothrow_destructible<value_type>::value){
		if(m_dirty){
			std::destroy_at(&m_storage.data);
			m_dirty = 0;
		}
	}

	template<class T>
	constexpr storage_for<T>::operator reference(void)noexcept{
		return m_storage.data;
	}
	template<class T>
	constexpr storage_for<T>::operator const_reference(void)const noexcept{
		return m_storage.data;
	}
	template<class T>
	constexpr storage_for<T>::operator rvalue_reference(void)&& noexcept{
		return std::move(m_storage.data);
	}

	template<class T>
	constexpr auto storage_for<T>::get(void)noexcept -> reference{
		return m_storage.data;
	}
	template<class T>
	constexpr auto storage_for<T>::get(void)const noexcept -> const_reference{
		return m_storage.data;
	}

	template<class T>
	constexpr auto storage_for<T>::operator->(void)noexcept -> pointer{
		return &m_storage.data;
	}
	template<class T>
	constexpr auto storage_for<T>::operator->(void)const noexcept -> const_pointer{
		return &m_storage.data;
	}

	template<class T>
	constexpr auto storage_for<T>::operator*(void)noexcept -> reference{
		return m_storage.data;
	}
	template<class T>
	constexpr auto storage_for<T>::operator*(void)const noexcept -> const_reference{
		return m_storage.data;
	}

	template<class T>
	constexpr bool storage_for<T>::valid(void)const{
		return m_dirty;
	}

}

#else //__cplusplus

namespace rexy{

	template<class T>
	template<class... Args>
	constexpr storage_for<T>::storage_for(Args&&... args)noexcept(std::is_nothrow_constructible<value_type,Args...>::value){
		new (m_storage) T(std::forward<Args>(args)...);
		m_dirty = 1;
	}
	template<class T>
	constexpr storage_for<T>::storage_for(const_reference t)noexcept(std::is_nothrow_copy_constructible<value_type>::value){
		new (m_storage) T(t);
		m_dirty = 1;
	}
	template<class T>
	constexpr storage_for<T>::storage_for(rvalue_reference t)noexcept(std::is_nothrow_move_constructible<value_type>::value){
		new (m_storage) T(std::move(t));
		m_dirty = 1;
	}
	template<class T>
	constexpr storage_for<T>::storage_for(const storage_for& s)noexcept(std::is_nothrow_copy_constructible<value_type>::value){
		new (m_storage) T(reinterpret_cast<const T&>(s.m_storage));
		m_dirty = 1;
	}
	template<class T>
	constexpr storage_for<T>::storage_for(storage_for&& s)noexcept(std::is_nothrow_move_constructible<value_type>::value){
		new (m_storage) T(std::move(reinterpret_cast<T&>(&s.m_storage)));
		m_dirty = 1;
	}
	template<class T>
	REXY_CPP20_CONSTEXPR storage_for<T>::~storage_for(void)noexcept(std::is_nothrow_destructible<value_type>::value){
		if(m_dirty){
			destroy();
		}
	}

	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(const storage_for& s)noexcept(std::is_nothrow_copy_assignable<value_type>::value){
		reinterpret_cast<T&>(m_storage) = static_cast<const T&>(s);
		return *this;
	}
	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(storage_for&& s)noexcept(std::is_nothrow_move_assignable<value_type>::value){
		reinterpret_cast<T&>(m_storage) = static_cast<T&&>(std::move(s));
		return *this;
	}
	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(const_reference t)noexcept(std::is_nothrow_copy_assignable<value_type>::value){
		reinterpret_cast<T&>(m_storage) = t;
		return *this;
	}
	template<class T>
	constexpr storage_for<T>& storage_for<T>::operator=(rvalue_reference t)noexcept(std::is_nothrow_move_assignable<value_type>::value){
		reinterpret_cast<T&>(m_storage) = std::move(t);
		return *this;
	}
	template<class T>
	constexpr void storage_for<T>::destroy(void)noexcept(std::is_nothrow_destructible<value_type>::value){
		reinterpret_cast<T*>(&m_storage)->~T();
		m_dirty = 0;
	}

	template<class T>
	constexpr storage_for<T>::operator reference(void)noexcept{
		return reinterpret_cast<reference>(m_storage);
	}
	template<class T>
	constexpr storage_for<T>::operator const_reference(void)const noexcept{
		return reinterpret_cast<const_reference>(m_storage);
	}
	template<class T>
	constexpr storage_for<T>::operator rvalue_reference(void)&& noexcept{
		return reinterpret_cast<rvalue_reference>(std::move(m_storage));
	}

	template<class T>
	constexpr auto storage_for<T>::get(void)noexcept -> reference{
		return reinterpret_cast<reference>(m_storage);
	}
	template<class T>
	constexpr auto storage_for<T>::get(void)const noexcept -> const_reference{
		return reinterpret_cast<const_reference>(m_storage);
	}

	template<class T>
	constexpr auto storage_for<T>::operator->(void)noexcept -> pointer{
		return reinterpret_cast<T*>(&m_storage);
	}
	template<class T>
	constexpr auto storage_for<T>::operator->(void)const noexcept -> const_pointer{
		return reinterpret_cast<const T*>(&m_storage);
	}

	template<class T>
	constexpr auto storage_for<T>::operator*(void)noexcept -> reference{
		return reinterpret_cast<T&>(m_storage);
	}
	template<class T>
	constexpr auto storage_for<T>::operator*(void)const noexcept -> const_reference{
		return reinterpret_cast<const T&>(m_storage);
	}

	template<class T>
	constexpr bool storage_for<T>::valid(void)const{
		return m_dirty;
	}

}

#endif //__cplusplus

#endif
