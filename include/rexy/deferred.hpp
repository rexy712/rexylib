/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_UTIL_DEFERRED_HPP
#define REXY_UTIL_DEFERRED_HPP

#include <tuple>
#include <utility> //forward, index_sequence
#include <cstddef> //size_t

#include "compat/standard.hpp"

namespace rexy{
	template<class T, class... Args>
	class deferred
	{
	public:
		using value_type = T;

	private:
		std::tuple<Args&&...> m_args;

	public:
		template<class... FArgs>
		constexpr deferred(FArgs&&... args);

		REXY_CPP20_CONSTEXPR deferred(const deferred&) = default;
		REXY_CPP20_CONSTEXPR deferred(deferred&&) = default;
		REXY_CPP20_CONSTEXPR ~deferred(void) = default;
		REXY_CPP20_CONSTEXPR deferred& operator=(const deferred&) = default;
		REXY_CPP20_CONSTEXPR deferred& operator=(deferred&&) = default;

		constexpr value_type value(void);
		constexpr operator value_type(void);

	private:
		template<std::size_t... Is>
		constexpr value_type get_value_(std::index_sequence<Is...>);
	};

	template<class Fn, class... Args>
	class deferred_function
	{
	public:
		using ret_t = decltype(std::declval<Fn>()(std::declval<Args>()...));

	private:
		Fn m_fn;
		std::tuple<Args&&...> m_args;

	public:
		template<class F, class... FArgs>
		constexpr deferred_function(F&& f, FArgs&&... args);

		constexpr decltype(auto) operator()(void);

		constexpr decltype(auto) value(void);
		constexpr operator ret_t(void);

	private:
		template<std::size_t... Is>
		constexpr decltype(auto) get_value_(std::index_sequence<Is...>);
	};

	template<class T, class... Args>
	template<class... FArgs>
	constexpr deferred<T,Args...>::deferred(FArgs&&... args):
		m_args{std::forward<FArgs>(args)...}{}
	template<class T, class... Args>
	constexpr auto deferred<T,Args...>::value(void) -> value_type{
		return get_value_(std::make_index_sequence<sizeof...(Args)>());
	}
	template<class T, class... Args>
	constexpr deferred<T,Args...>::operator value_type(void){
		return get_value_(std::make_index_sequence<sizeof...(Args)>());
	}
	template<class T, class... Args>
	template<std::size_t... Is>
	constexpr auto deferred<T,Args...>::get_value_(std::index_sequence<Is...>) -> value_type{
		return T{std::forward<std::tuple_element_t<Is,decltype(m_args)>>(std::get<Is>(m_args))...};
	}

	template<class Fn, class... Args>
	template<class F, class... FArgs>
	constexpr deferred_function<Fn,Args...>::deferred_function(F&& f, FArgs&&... args):
		m_fn(std::forward<F>(f)),
		m_args{std::forward<FArgs>(args)...}{}

	template<class Fn, class... Args>
	constexpr decltype(auto) deferred_function<Fn,Args...>::operator()(void){
		return get_value_(std::make_index_sequence<sizeof...(Args)>());
	}

	template<class Fn, class... Args>
	constexpr decltype(auto) deferred_function<Fn,Args...>::value(void){
		return get_value_(std::make_index_sequence<sizeof...(Args)>());
	}
	template<class Fn, class... Args>
	constexpr deferred_function<Fn,Args...>::operator ret_t(void){
		return get_value_(std::make_index_sequence<sizeof...(Args)>());
	}
	template<class Fn, class... Args>
	template<std::size_t... Is>
	constexpr decltype(auto) deferred_function<Fn,Args...>::get_value_(std::index_sequence<Is...>){
		return std::forward<Fn>(m_fn)(std::forward<std::tuple_element_t<Is,decltype(m_args)>>(std::get<Is>(m_args))...);
	}

	template<class Fn, class... Args>
	deferred_function(Fn&&, Args&&...) -> deferred_function<Fn&&, Args&&...>;


	template<class T, class... Args>
	deferred<T,Args...> make_deferred(Args&&... args){
		return deferred<T,Args...>(std::forward<Args>(args)...);
	}

}

#endif
