/**
	This file is a part of rexy's general purpose library
	Copyright (C) 2020-2022 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXY_STRING_BASE_HPP
#define REXY_STRING_BASE_HPP

#include <type_traits> //is_same, integral_contant, enable_if, etc
#include <utility> //forward
#include <cstddef> //std::size_t,ptrdiff
#include <climits> //CHAR_BIT
#include <iterator> //reverse_iterator
#include <initializer_list>

#include "steal.hpp"
#include "utility.hpp" //memcpy
#include "traits.hpp"
#include "expression.hpp"
#include "detail/string_appender.hpp"
#include "detail/hasallocator.hpp"
#include "allocator.hpp"
#include "rexy.hpp"

#include "compat/standard.hpp"

namespace rexy{

#ifndef LIBREXY_ENABLE_SSO
	#define LIBREXY_ENABLE_SSO 1
#endif

	template<class Char>
	class basic_string_view;

	//Base of all RAII strings. Its use is allowing passing of rexy strings to functions without knowing the exact type
	template<class Char>
	class string_base
	{
	public:
		using value_type = Char;
		using size_type = std::size_t;
		using difference_type = ptrdiff_t;
		using pointer = value_type*;
		using const_pointer = const value_type*;
		using reference = value_type&;
		using const_reference = const value_type&;
		using iterator = pointer;
		using const_iterator = const_pointer;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

#if LIBREXY_ENABLE_SSO != 0
	private:
		static constexpr size_type EXTRA_SDATA_LEN = 0;

		//represent long string
		struct ldata{
			unsigned char islong:1; //common subsequence with short string
			size_type capacity:(CHAR_BIT*sizeof(size_type)-1); //take away last bit from capacity for islong
			size_type length; //length of string excluding null terminator
			constexpr ldata(void)noexcept:
				islong(1),
				capacity(0),
				length(0){}
		};

		static constexpr size_type MAX_SHORT_LEN = EXTRA_SDATA_LEN+sizeof(ldata)-2;
		//represent short string
		struct sdata{
			unsigned char islong:1; //common subsequence with long string
			unsigned char length:(CHAR_BIT-1); //take away last bit from length for islong, excludes null terminator
			value_type data[MAX_SHORT_LEN+1]; //char array for string storage
			constexpr sdata(void)noexcept:
				islong(0),
				length(0),
				data{}{}
		};
		//union of short and long string representations.
		union combine_data{
			ldata l;
			sdata s;
			constexpr combine_data(void)noexcept:
				s(){}
		}m_data;

		//direct access to current string data regardless of representation. Increases access speed.
		pointer m_raw = m_data.s.data;

	protected:
		//Functions for handling long vs short string manipulation. Use this instead of directly modifying m_data.
		constexpr void set_islong_flag(bool b){
			//although well defined to set either one at any time, constexpr functions cannot change active union member.
			if(b)
				m_data.l.islong = b;
			else
				m_data.s.islong = b;
		}
		constexpr bool islong(void)const{
			//common standard layout union member subsequence, never undefined behavior
			return m_data.l.islong;
		}
		constexpr pointer set_short_ptr(void){
			set_islong_flag(false);
			return m_raw = m_data.s.data;
		}
		constexpr pointer set_long_ptr(pointer ptr){
			set_islong_flag(true);
			return m_raw = ptr;
		}
		constexpr pointer get_long_ptr(void){return m_raw;}
		constexpr pointer get_short_ptr(void){return m_raw;}
		constexpr const_pointer get_long_ptr(void)const{return m_raw;}
		constexpr const_pointer get_short_ptr(void)const{return m_raw;}
		constexpr pointer get_pointer(void){return m_raw;}
		constexpr const_pointer get_pointer(void)const{return m_raw;}
		constexpr void set_long_length(size_type len){m_data.l.length = len;}
		constexpr size_type get_long_length(void)const{return m_data.l.length;}
		constexpr void set_short_length(size_type len){m_data.s.length = static_cast<unsigned char>(len);}
		constexpr size_type get_short_length(void)const{return m_data.s.length;}
		constexpr void set_long_capacity(size_type cap){m_data.l.capacity = cap;}
		constexpr void set_short_capacity(size_type){}
		constexpr size_type get_long_capacity(void)const{return m_data.l.capacity;}
		constexpr size_type get_short_capacity(void)const{return MAX_SHORT_LEN;}
		constexpr void set_capacity(size_type s){
			if(islong())
				set_long_capacity(s);
			else
				set_short_capacity(s);
		}
		constexpr void set_length(size_type s){
			if(islong())
				set_long_length(s);
			else
				set_short_length(s);
		}

	protected:
		constexpr string_base(void)noexcept = default;
		//Initialize without copying
		constexpr string_base(pointer data, size_type len, size_type cap)noexcept{
			if(cap > MAX_SHORT_LEN){
				set_islong_flag(true);
				set_long_ptr(data);
				set_long_length(len);
				set_long_capacity(cap);
			}else if(len){
				set_islong_flag(false);
				pointer raw = set_short_ptr();
				rexy::memcpy(raw, data, sizeof(value_type)*len);
				raw[len] = 0;
				set_short_length(len);
				set_short_capacity(cap);
			}
		}
		constexpr string_base(pointer data, size_type len)noexcept:
			string_base(data, len, len){}
		//Copy ctor, copy length+capacity+short string, not long string value
		constexpr string_base(const string_base& s)noexcept:
			m_data(s.m_data){}
		constexpr string_base(string_base&& s)noexcept:
			m_data(std::move(s.m_data)),
			m_raw(s.islong() ? s.m_raw : m_data.s.data)
		{
			s.set_islong_flag(false);
		}
		REXY_CPP20_CONSTEXPR ~string_base(void)noexcept = default;
		constexpr string_base& operator=(string_base&& s)noexcept{
			std::swap(m_data, s.m_data);
			if(this->islong())
				std::swap(m_raw, s.m_raw);
			else{
				s.m_raw = m_raw;
				m_raw = m_data.s.data;
			}
			return *this;
		}
	public:
		constexpr size_type length(void)const noexcept{
			if(islong())
				return get_long_length();
			else
				return get_short_length();
		}
		constexpr size_type capacity(void)const noexcept{
			if(islong())
				return get_long_capacity();
			else
				return get_short_capacity();
		}
		static constexpr bool uses_sso(void){return true;}
		static constexpr size_type short_string_size(void){return MAX_SHORT_LEN;}
#else //LIBREXY_ENABLE_SSO
	private:
		//direct access to current string
		pointer m_raw = nullptr;
		size_type m_length = 0;
		size_type m_capacity = 0;

	protected:
		constexpr pointer get_pointer(void){return m_raw;}
		constexpr const_pointer get_pointer(void)const{return m_raw;}
		constexpr void set_length(size_type s){m_length = s;}
		constexpr void set_capacity(size_type s){m_capacity = s;}
		constexpr bool islong(void)const{return true;}
		constexpr pointer set_short_ptr(void){return m_raw;}
		constexpr pointer set_long_ptr(pointer ptr){return m_raw = ptr;}

	protected:
		constexpr string_base(void)noexcept = default;
		//Initialize without copying
		constexpr string_base(pointer data, size_type len, size_type cap)noexcept:
			m_raw(data),
			m_length(len),
			m_capacity(cap){}
		constexpr string_base(pointer data, size_type len)noexcept:
			string_base(data, len, len){}
		//Copy ctor, copy length+capacity+short string, not long string value
		constexpr string_base(const string_base& s)noexcept:
			m_raw(s.m_raw),
			m_length(s.m_length),
			m_capacity(s.m_capacity){}
		constexpr string_base(string_base&& s)noexcept:
			m_raw(std::exchange(s.m_raw, nullptr)),
			m_length(s.m_length),
			m_capacity(s.m_capacity){}
		REXY_CPP20_CONSTEXPR ~string_base(void)noexcept = default;
		constexpr string_base& operator=(string_base&& s)noexcept{
			std::swap(m_raw, s.m_raw);
			m_length = s.m_length;
			m_capacity = s.m_capacity;
			return *this;
		}
	public:
		constexpr size_type length(void)const noexcept{
			return m_length;
		}
		constexpr size_type capacity(void)const noexcept{
			return m_capacity;
		}
		static constexpr bool uses_sso(void){return false;}
		static constexpr size_type short_string_size(void){return 0;}
#endif //LIBREXY_ENABLE_SSO
	public:
		//Length of string not including null terminator
		constexpr size_type size(void)const noexcept{
			return length();
		}
		constexpr size_type max_size(void)const noexcept{
			return size_type{-2};
		}
		//direct access to managed pointer
		constexpr pointer c_str(void)noexcept{return get_pointer();}
		constexpr const_pointer c_str(void)const noexcept{return get_pointer();}
		constexpr pointer data(void)noexcept{return get_pointer();}
		constexpr const_pointer data(void)const noexcept{return get_pointer();}
		constexpr operator basic_string_view<value_type>(void)const noexcept{return basic_string_view<value_type>(begin(), end());}

		//true if m_data is not empty
		constexpr bool valid(void)const noexcept{return length() > 0;}
		constexpr bool empty(void)const noexcept{return length() == 0;}

		constexpr reference operator[](size_type i)noexcept{return get_pointer()[i];}
		constexpr const_reference operator[](size_type i)const noexcept{return get_pointer()[i];}
		constexpr reference at(size_type i)noexcept{return get_pointer()[i];}
		constexpr const_reference at(size_type i)const noexcept{return get_pointer()[i];}

		constexpr const_iterator search(basic_string_view<value_type> sv)const noexcept;
		constexpr iterator search(basic_string_view<value_type> sv)noexcept;
		constexpr const_iterator search(const_pointer c)const noexcept;
		constexpr iterator search(const_pointer c)noexcept;
		template<class Searcher>
		constexpr const_iterator search(const_pointer c, const Searcher& searcher)const noexcept(
				std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_pointer, const_pointer>);
		template<class Searcher>
		constexpr iterator search(const_pointer c, const Searcher& searcher)noexcept(
				std::is_nothrow_invocable_v<Searcher, iterator, iterator, const_pointer, const_pointer>);

		constexpr const_iterator rsearch(basic_string_view<value_type> sv)const noexcept;
		constexpr iterator rsearch(basic_string_view<value_type> sv)noexcept;
		constexpr const_iterator rsearch(const_pointer c)const noexcept;
		constexpr iterator rsearch(const_pointer c)noexcept;
		template<class Searcher>
		constexpr const_iterator rsearch(const_pointer c, const Searcher& searcher)const noexcept(
				std::is_nothrow_invocable_v<Searcher, const_iterator, const_iterator, const_pointer, const_pointer>);
		template<class Searcher>
		constexpr iterator rsearch(const_pointer c, const Searcher& searcher)noexcept(
				std::is_nothrow_invocable_v<Searcher, iterator, iterator, const_pointer, const_pointer>);

		constexpr bool starts_with(basic_string_view<value_type> sv)const noexcept;
		constexpr bool starts_with(value_type v)const noexcept;
		constexpr bool starts_with(const_pointer str)const noexcept;

		constexpr bool ends_with(basic_string_view<value_type> sv)const noexcept;
		constexpr bool ends_with(value_type v)const noexcept;
		constexpr bool ends_with(const_pointer str)const noexcept;

		constexpr bool contains(basic_string_view<value_type> sv)const noexcept;
		constexpr bool contains(value_type v)const noexcept;
		constexpr bool contains(const_pointer str)const noexcept;

		//TODO more compares
		constexpr bool compare(const string_base& s)const noexcept{return *this == s;}
		constexpr bool compare(basic_string_view<value_type> s)const noexcept{return *this == s;}
		constexpr bool compare(const_pointer c)const noexcept{return *this == c;}

		constexpr size_type find_first_of(value_type v, size_type start = 0)const noexcept;
		constexpr size_type find_first_of(const_pointer c, size_type start = 0)const noexcept;
		constexpr size_type find_first_of(const_pointer c, size_type start, size_type size)const noexcept;
		template<class StringView>
		constexpr auto find_first_of(const StringView& str, size_type start = 0)const noexcept(
				std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
					std::enable_if_t<
						std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
						size_type>;

		constexpr size_type find_last_of(value_type v, size_type start = 0)const noexcept;
		constexpr size_type find_last_of(const_pointer c, size_type start = 0)const noexcept;
		constexpr size_type find_last_of(const_pointer c, size_type start, size_type size)const noexcept;
		template<class StringView>
		constexpr auto find_last_of(const StringView& str, size_type start = 0)const noexcept(
				std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
					std::enable_if_t<
						std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
						size_type>;

		constexpr size_type find_first_not_of(const_pointer str, size_type start, size_type count)const noexcept;
		constexpr size_type find_first_not_of(const_pointer str, size_type start = 0)const noexcept;
		constexpr size_type find_first_not_of(value_type v, size_type start = 0)const noexcept;
		template<class StringView>
		constexpr auto find_first_not_of(const StringView& str, size_type start = 0)const noexcept(
				std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
					std::enable_if_t<
						std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
						size_type>;

		constexpr size_type find_last_not_of(const_pointer str, size_type start, size_type count)const noexcept;
		constexpr size_type find_last_not_of(const_pointer str, size_type start = 0)const noexcept;
		constexpr size_type find_last_not_of(value_type v, size_type start = 0)const noexcept;
		template<class StringView>
		constexpr auto find_last_not_of(const StringView& str, size_type start = 0)const noexcept(
				std::is_nothrow_convertible_v<const StringView&,basic_string_view<value_type>>) ->
					std::enable_if_t<
						std::is_convertible_v<const StringView&, basic_string_view<value_type>>,
						size_type>;

		constexpr reference front(void)noexcept{return *get_pointer();}
		constexpr const_reference front(void)const noexcept{return *get_pointer();}
		constexpr reference back(void)noexcept{return *(get_pointer() + length() - 1);}
		constexpr const_reference back(void)const noexcept{return *(get_pointer() + length() - 1);}

		constexpr iterator begin(void)noexcept{return get_pointer();}
		constexpr const_iterator begin(void)const noexcept{return get_pointer();}
		constexpr iterator end(void)noexcept{return get_pointer()+length();}
		constexpr const_iterator end(void)const noexcept{return get_pointer()+length();}
		constexpr const_iterator cbegin(void)const noexcept{return begin();}
		constexpr const_iterator cend(void)const noexcept{return end();}

		constexpr reverse_iterator rbegin(void)noexcept{return reverse_iterator(get_pointer()+length());}
		constexpr const_reverse_iterator rbegin(void)const noexcept{return const_reverse_iterator(get_pointer()+length());}
		constexpr reverse_iterator rend(void)noexcept{return reverse_iterator(get_pointer());}
		constexpr const_reverse_iterator rend(void)const noexcept{return const_reverse_iterator(get_pointer());}
		constexpr const_reverse_iterator crbegin(void)const noexcept{return rbegin();}
		constexpr const_reverse_iterator crend(void)const noexcept{return rend();}

		constexpr void clear(void)noexcept;

		constexpr basic_string_view<value_type> create_view(void)const noexcept;
		constexpr basic_string_view<value_type> create_view(const_iterator start, const_iterator fin)const noexcept;

	}; //class string_base


	//Supplies all functions that string_base can't implement
	template<class Char, REXY_ALLOCATOR_CONCEPT Alloc = rexy::allocator<Char>>
	class basic_string : protected detail::hasallocator<Alloc>, public string_base<Char>
	{
	public:
		using value_type = typename string_base<Char>::value_type;
		using size_type = typename string_base<Char>::size_type;
		using difference_type = typename string_base<Char>::difference_type;
		using pointer = typename string_base<Char>::pointer;
		using const_pointer = typename string_base<Char>::const_pointer;
		using reference = typename string_base<Char>::reference;
		using const_reference = typename string_base<Char>::const_reference;
		using iterator = typename string_base<Char>::iterator;
		using const_iterator = typename string_base<Char>::const_iterator;
		using reverse_iterator = typename string_base<Char>::reverse_iterator;
		using const_reverse_iterator = typename string_base<Char>::const_reverse_iterator;
		using allocator_type = Alloc;

		static constexpr size_type npos = size_type(-1);

	private:

		REXY_CPP20_CONSTEXPR
		void _copy_construct_string(const_pointer data, size_type len, size_type cap)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& _copy_string(const_pointer s, size_type len)noexcept(
				is_nothrow_allocator_v<Alloc>);

		template<class InputIt>
		REXY_CPP20_CONSTEXPR
		basic_string& _insert_impl(size_type pos, InputIt start, size_type insert_count)noexcept(
				is_nothrow_allocator_v<Alloc>);

		template<class InputIt>
		constexpr basic_string& _replace_impl(size_type pos, size_type count, InputIt src, size_type count2)noexcept;

	public:
		constexpr basic_string(void)noexcept;
		constexpr basic_string(rexy::steal<pointer> data, size_type len)noexcept;
		constexpr basic_string(rexy::steal<pointer> data, size_type len, size_type cap)noexcept;
		constexpr basic_string(rexy::steal<pointer> data)noexcept;

		REXY_CPP20_CONSTEXPR
		basic_string(const_pointer data, size_type len)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string(const_pointer data, size_type len, size_type cap)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string(const_pointer data)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		explicit basic_string(size_type len)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string(size_type len, size_type cap)noexcept(
				is_nothrow_allocator_v<Alloc>);

		template<class InputIt, class Enable = std::enable_if_t<is_legacy_input_iterator_v<InputIt>,int>>
		REXY_CPP20_CONSTEXPR
		basic_string(InputIt start, InputIt fin)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string(const basic_string_view<Char>& sv)noexcept(
				is_nothrow_allocator_v<Alloc>);

		//normal copy and move ctors
		REXY_CPP20_CONSTEXPR
		basic_string(const basic_string& b)noexcept(
				is_nothrow_allocator_v<Alloc>);

		constexpr basic_string(basic_string&& s)noexcept;

		REXY_CPP20_CONSTEXPR
		basic_string(const string_base<Char>&)noexcept(
				is_nothrow_allocator_v<Alloc>);


		//dtor
		REXY_CPP20_CONSTEXPR
		~basic_string(void)noexcept(
				is_nothrow_allocator_v<Alloc>);


		REXY_CPP20_CONSTEXPR
		basic_string& operator=(const basic_string& s)noexcept(
				is_nothrow_allocator_v<Alloc>);

		constexpr basic_string& operator=(basic_string&& s)noexcept;

		REXY_CPP20_CONSTEXPR
		basic_string& operator=(const string_base<Char>& s)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& operator=(const basic_string_view<Char>& sv)noexcept(
				is_nothrow_allocator_v<Alloc>);

		//Copy from c string
		REXY_CPP20_CONSTEXPR
		basic_string& operator=(const_pointer c)noexcept(
				is_nothrow_allocator_v<Alloc>);


		//These can't be put in base because they require us to know the type of Alloc
		constexpr size_type find_first_of(const basic_string& str, size_type start = 0)const noexcept;
		constexpr size_type find_last_of(const basic_string& str, size_type start = 0)const noexcept;
		constexpr size_type find_first_not_of(const basic_string& str, size_type start = 0)const noexcept;
		constexpr size_type find_last_not_of(const basic_string& str, size_type start = 0)const noexcept;

		//Replace managed pointer. Frees existing value
		REXY_CPP20_CONSTEXPR
		void reset(pointer val = nullptr)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		void reset(pointer val, size_type len)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		bool reserve(size_type newsize)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		void shrink_to_fit(void)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		void resize(size_type newsize, value_type v = value_type())noexcept(
				is_nothrow_allocator_v<Alloc>);



		REXY_CPP20_CONSTEXPR
		basic_string& insert(size_type pos, size_type insert_count, value_type v)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& insert(size_type pos, value_type v)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& insert(size_type pos, const_pointer str)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& insert(size_type pos, const_pointer str, size_type insert_count)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& insert(size_type pos, const basic_string& other)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& insert(size_type pos, const basic_string& other, size_type index_str, size_type count = npos)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& insert(const_iterator pos, value_type v)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& insert(const_iterator pos, size_type count, value_type v)noexcept(
				is_nothrow_allocator_v<Alloc>);

		template<class InputIt>
		REXY_CPP20_CONSTEXPR
		auto insert(const_iterator pos, InputIt start, InputIt last)noexcept(
					is_nothrow_allocator_v<Alloc>) ->
			std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>;

		template<class InputIt>
		REXY_CPP20_CONSTEXPR
		auto insert(size_type pos, InputIt start, InputIt last)noexcept(
					is_nothrow_allocator_v<Alloc>) ->
			std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>;

		REXY_CPP20_CONSTEXPR
		basic_string& insert(const_iterator pos, std::initializer_list<value_type> list)noexcept(
				is_nothrow_allocator_v<Alloc>);

		template<class StringView>
		REXY_CPP20_CONSTEXPR
		auto insert(size_type pos, const StringView& sv)noexcept(
				is_nothrow_allocator_v<Alloc>) ->
					std::enable_if_t<
						std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
						!std::is_convertible_v<const StringView&, const_pointer>,
						basic_string&>;

		template<class StringView>
		REXY_CPP20_CONSTEXPR
		auto insert(size_type pos, const StringView& sv, size_type index_str, size_type count = npos)noexcept(
				is_nothrow_allocator_v<Alloc>) ->
					std::enable_if_t<
						std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
						!std::is_convertible_v<const StringView&,const_pointer>,
						basic_string&>;



		template<class InputIt>
		REXY_CPP20_CONSTEXPR
		auto append(InputIt start, InputIt fin)noexcept(
				is_nothrow_allocator_v<Alloc>) ->
					std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>;

		REXY_CPP20_CONSTEXPR
		basic_string& append(const_pointer data, size_type len)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& append(const_pointer data)noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		basic_string& append(const basic_string& other)noexcept(
				is_nothrow_allocator_v<Alloc>);



		REXY_CPP20_CONSTEXPR
		void push_back(value_type data)noexcept(
				is_nothrow_allocator_v<Alloc>);

		constexpr void pop_back(void)noexcept;


		constexpr basic_string& replace(size_type pos, size_type count, const basic_string& str)noexcept;
		constexpr basic_string& replace(const_iterator pos, const_iterator last, const basic_string& str)noexcept;
		constexpr basic_string& replace(size_type pos, size_type count, const basic_string& str, size_type pos2, size_type count2 = npos)noexcept;

		template<class InputIt>
		constexpr auto replace(const_iterator first, const_iterator last, InputIt first2, InputIt last2)noexcept ->
				std::enable_if_t<is_legacy_input_iterator_v<InputIt>,basic_string&>;

		constexpr basic_string& replace(size_type pos, size_type count, const_pointer cstr, size_type count2)noexcept;
		constexpr basic_string& replace(const_iterator first, const_iterator last, const_pointer cstr, size_type count)noexcept;
		constexpr basic_string& replace(size_type pos, size_type count, const_pointer cstr)noexcept;
		constexpr basic_string& replace(const_iterator first, const_iterator last, const_pointer cstr)noexcept;
		constexpr basic_string& replace(size_type pos, size_type count, size_type count2, value_type v)noexcept;
		constexpr basic_string& replace(const_iterator first, const_iterator last, size_type count2, value_type v)noexcept;
		constexpr basic_string& replace(const_iterator first, const_iterator last, std::initializer_list<value_type> list)noexcept;

		template<class StringView>
		constexpr auto replace(size_type pos, size_type count, const StringView& sv)noexcept ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
					!std::is_convertible_v<const StringView&,const_pointer>,
					basic_string&>;

		template<class StringView>
		constexpr auto replace(const_iterator first, const_iterator last, const StringView& sv)noexcept ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
					!std::is_convertible_v<const StringView&,const_pointer>,
					basic_string&>;

		template<class StringView>
		constexpr auto replace(size_type pos, size_type count, const StringView& sv, size_type pos2, size_type count2 = npos)noexcept ->
				std::enable_if_t<
					std::is_convertible_v<const StringView&, basic_string_view<value_type>> &&
					!std::is_convertible_v<const StringView&,const_pointer>,
					basic_string&>;


		constexpr basic_string& erase(size_type index = 0, size_type count = npos)noexcept;
		constexpr iterator erase(const_iterator pos)noexcept;
		constexpr iterator erase(const_iterator first, const_iterator last)noexcept;


		template<REXY_ALLOCATOR_CONCEPT A = allocator_type>
		REXY_CPP20_CONSTEXPR
		basic_string<value_type,A> substring(size_type start, size_type end)const noexcept(
				is_nothrow_allocator_v<A>);

		REXY_CPP20_CONSTEXPR
		basic_string substr(size_type start, size_type end)const noexcept(
				is_nothrow_allocator_v<Alloc>);

		REXY_CPP20_CONSTEXPR
		pointer release(void)noexcept(
				is_nothrow_allocator_v<Alloc>);

		using detail::hasallocator<Alloc>::allocator;

	}; //class basic_string

	//Like an expression template but not really
	template<class Left, class Right>
	class string_cat_expr : public rexy::binary_expression<Left,Right>
	{
	static_assert(std::is_same<typename std::decay_t<Left>::value_type,typename std::decay_t<Right>::value_type>::value);
	private:
		using left_t = std::decay_t<Left>;
		using right_t = std::decay_t<Right>;
	public:
		using value_type = typename left_t::value_type;
		using size_type = decltype(typename left_t::size_type{0} + typename right_t::size_type{0});
		using difference_type = decltype(typename left_t::difference_type{0} - typename right_t::difference_type{0});
		using pointer = value_type*;
		using const_pointer = const value_type*;
		using reference = value_type&;
		using const_reference = const value_type&;
		using iterator = value_type*;
		using const_iterator = const value_type*;

	public:
		using binary_expression<Left,Right>::binary_expression;

		constexpr string_cat_expr(const string_cat_expr&) = default;
		constexpr string_cat_expr(string_cat_expr&&) = default;

		constexpr size_type length(void)const noexcept;
		template<REXY_ALLOCATOR_CONCEPT Alloc>
		REXY_CPP20_CONSTEXPR
		operator basic_string<value_type,Alloc>(void)noexcept(
				std::is_nothrow_constructible_v<
					basic_string<value_type,Alloc>,
					typename basic_string<value_type,Alloc>::size_type> &&
				std::is_nothrow_invocable_v<
					detail::string_appender<basic_string<value_type,Alloc>>,
					decltype(*this)>);
	};
	template<class Left, class Right>
	string_cat_expr(Left&&,Right&&) -> string_cat_expr<Left&&,Right&&>;

} //namespace rexy

#include "string_base.tpp"

#endif
